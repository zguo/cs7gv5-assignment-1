#pragma once
#ifndef CG_CAMERA_H
#define CG_CAMERA_H

#include "common.h"

#include <cmath>

#include "Enum.h"
#include "GameObject.h"

namespace cg
{
	const float YAW = -90.0f;
	const float PITCH = 0.0f;
	const float SPEED = 1.0f;
	const float SENSITIVITY = 0.1f;
	const float FOV = 45.0f;
	class PlayerController;
	// TODO: check if need to separate camera and camera components
	class Camera : public GameObject
	{
	public:
		Camera() :
			position(glm::vec3(0.0f, 0.0f, 0.0f)),
			up(glm::vec3(0.0f, 1.0f, 0.0f)),
			yaw(YAW),
			pitch(PITCH),
			front(glm::vec3(0.0f, 0.0f, -1.0f)),
			movement_speed(SPEED),
			mouse_sensitivity(SENSITIVITY),
			fov(FOV),
			first_mouse(true),
			mode(cg::ECameraMode::FREE),
			CursorPosCallback(nullptr),
			ScrollCallback(nullptr)
		{}
		Camera(
			glm::vec3 position,
			cg::ECameraMode mode,
			glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f),
			glm::vec3 front = glm::vec3(0.0f, 0.0f, -1.0f),
			float yaw = YAW,
			float pitch = PITCH,
			float movement_speed = SPEED,
			float mouse_sensitivity = SENSITIVITY,
			float fov = FOV
		) : position(position),
			up(up),
			yaw(yaw),
			pitch(pitch),
			front(front),
			movement_speed(movement_speed),
			mouse_sensitivity(mouse_sensitivity),
			fov(fov),
			first_mouse(true),
			mode(mode),
			CursorPosCallback(nullptr),
			ScrollCallback(nullptr)
		{}

		glm::mat4 GetViewMatrix();
		void ProcessKeyboard(cg::EPlayerMovement direction, float delta_time);
		float GetYaw() const { return yaw; }
		void SetYaw(float val) { yaw = val; }
		float GetPitch() const { return pitch; }
		void SetPitch(float val) { pitch = val; }
		glm::vec3 GetFront() const { return front; }
		void SetFront(glm::vec3 val) { front = val; }
		float GetFOV() const { return fov; }
		void SetFOV(float val) { fov = val; }
		glm::vec3 GetPosition() const { return position; }
		void SetPosition(glm::vec3 val) { position = val; }
		glm::vec3 GetUp() const { return up; }
		void SetUp(glm::vec3 val) { up = val; }
		glm::vec3 GetRight() const { return this->right; }
		void SetRight(glm::vec3 val) { this->right = val; }
		bool GetFirstMouse() const { return this->first_mouse; }
		void SetFirstMouse(bool value) { this->first_mouse = value; }
		float GetLastX() const { return this->last_x; }
		void SetLastX(float value) { this->last_x = value; }
		float GetLastY() const { return this->last_y; }
		void SetLastY(float value) { this->last_y = value; }
		cg::ECameraMode GetMode() const { return this->mode; }
		glm::vec3 GetTarget() const { return this->target; }
		void SetTarget(glm::vec3 val) { this->target = val; }
		glm::vec3 GetOffset() const { return this->offset; }
		float GetDistance() const { return this->distance; }
		void SetDistance(float distance) { this->distance = distance; }
		void SetOffset(glm::vec3 val) { this->offset = val; }
		void (*CursorPosCallback) (cg::PlayerController* self, GLFWwindow* window, double x_pos, double y_pos);
		void (*ScrollCallback)(cg::PlayerController* self, GLFWwindow* window, double x_offset, double y_offset);

	private:
		glm::vec3 position;
		glm::vec3 front;
		glm::vec3 up;
		glm::vec3 right;
		glm::vec3 world_up;
		glm::vec3 target;
		glm::vec3 offset;
		cg::ECameraMode mode;
		float distance;
		float yaw;
		float pitch;
		float movement_speed;
		float mouse_sensitivity;
		float fov;
		float last_x;
		float last_y;
		bool first_mouse;
	};
}
#endif	// CG_CAMERA_H