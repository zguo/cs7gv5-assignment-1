#pragma once
#ifndef CG_SKELETAL_MESH_ACTOR_H
#define CG_SKELETAL_MESH_ACTOR_H

#include "common.h"

#include "SceneActor.h"
#include "CDynamicMesh.h"

namespace cg
{
	class GameInstance;

	class DynamicModel : public SceneActor
	{
	public:
		DynamicModel() : forward(glm::vec3(0.0f, 0.0f, -1.0f)),
			up(glm::vec3(0.0f, 1.0f, 0.0f)),
			right(glm::vec3(1.0f, 0.0f, 0.0f))
		{
			this->renderer->ActivateContext();
		}

		DynamicModel(const std::string& path) :forward(glm::vec3(0.0f, 0.0f, -1.0f)),
			up(glm::vec3(0.0f, 1.0f, 0.0f)),
			right(glm::vec3(1.0f, 0.0f, 0.0f))
		{
			this->renderer->ActivateContext();
			this->AddMeshComponent(path);
		}

		std::shared_ptr<cg::CTransform> GetTransformComp() const { return this->transform_comp; }
		glm::vec3 GetForward() const { return this->forward; }
		void SetForward(glm::vec3 value) { this->forward = value; }
		glm::vec3 GetUp() const { return this->up; }
		void SetUp(glm::vec3 value) { this->up = value; }
		glm::vec3 GetRight() const { return this->right; }
		void SetRight(glm::vec3 value) { this->right = value; }

		glm::vec3 GetGlobalPosition() const { return this->GetLocalPosition(); }
		void SetGlobalPosition(glm::vec3 pos) { this->SetLocalPosition(pos); }
		void SetGlobalPosition(float x, float y, float z) { this->SetLocalPosition(x, y, z); }

		glm::vec3 GetLocalRotation() const { return this->mesh_comps[0]->GetLocalRotation(); }
		void SetLocalRotation(glm::vec3 rot) { this->mesh_comps[0]->SetLocalRotation(rot); }
		void SetLocalRotation(float x, float y, float z) { this->mesh_comps[0]->SetLocalRotation(x, y, z); }
		void RotateAround(float degree, glm::vec3 axis) { this->mesh_comps[0]->RotateAround(degree, axis); }

		glm::vec3 GetLocalScale() const { return this->mesh_comps[0]->GetLocalScale(); }
		void SetLocalScale(glm::vec3 scale) { this->mesh_comps[0]->SetLocalScale(scale); }
		void SetLocalScale(float x, float y, float z) { this->mesh_comps[0]->SetLocalScale(x, y, z); }

		// the first added mesh component will be the parent component for hierarchy transform
		std::shared_ptr<cg::DynamicMeshComponent> AddMeshComponent(const std::string& file_path)
		{
			auto comp = std::make_shared<cg::DynamicMeshComponent>(file_path);
			this->mesh_comps.push_back(comp);
			return comp;
		}

		std::shared_ptr<cg::DynamicMeshComponent> AddMeshComponent(std::shared_ptr<cg::DynamicMeshComponent> comp)
		{
			this->mesh_comps.push_back(comp);
			return comp;
		}

		void Render(float delta_time) override;

		void Update(float delta) override;

		static float yaw;
		static float roll;
		static float pitch;
		static bool use_quaternion;

	protected:
		glm::vec3 forward;
		glm::vec3 up; // fixed for simplicity, requires to be modified later
		glm::vec3 right;

		std::vector<std::shared_ptr<cg::DynamicMeshComponent>> mesh_comps;
		std::shared_ptr<cg::BoundingBox> bounding_box;
		glm::mat4 rotation_matrix = glm::mat4(1.0f);
		//glm::mat4 rotation_matrix = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));

		std::string GenerateVertexShaderCode();
		std::string GenerateFragmentShaderCode();

	private:
		bool cursor_disabled = true;
	};
}

#endif // CG_SKELETAL_MESH_ACTOR_H