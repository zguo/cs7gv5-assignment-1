#pragma once
#ifndef CG_STATIC_MESH_ACTOR_H
#define CG_STATIC_MESH_ACTOR_H

#include "common.h"

#include "SceneActor.h"
#include "CStaticMesh.h"

namespace cg
{
	class StaticMeshActor : public SceneActor
	{
	public:
		StaticMeshActor() = default;
		StaticMeshActor(const std::string &path);
		std::shared_ptr<cg::StaticMeshComponent> GetMeshComp() const { return this->mesh_comp; }
		void Render(float delta_time) override;

	private:
		std::shared_ptr<cg::StaticMeshComponent> mesh_comp;
		std::shared_ptr<cg::BoundingBox> bounding_box;
	};
}

#endif // CG_STATIC_MESH_ACTOR_H
