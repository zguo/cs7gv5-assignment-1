#pragma once
#ifndef CG_RENDERABLE_OBJECT_H
#define CG_RENDERABLE_OBJECT_H

#include "common.h"

#include "IRenderable.h"
#include "Actor.h"
#include "ITransformable.h"
#include "IPhysics.h"

namespace cg
{
	class SceneActor : public Actor, public IRenderable, public ITransformable, public IPhysics
	{
	public:
		SceneActor(bool b_use_default_renderer = true);
		SceneActor(std::shared_ptr<cg::Renderer> renderer) { this->renderer = renderer; }
		std::shared_ptr<cg::Renderer> GetRenderer() const { return this->renderer; }
		virtual void Render(float delta_time) override {}
	};
}

#endif // CG_RENDERABLE_OBJECT_H