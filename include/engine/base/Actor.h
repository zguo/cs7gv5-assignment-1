#pragma once
#ifndef CG_ACTOR_H
#define CG_ACTOR_H

#include "common.h"

#include "GameObject.h"
#include "ILifeCycleEvents.h"

namespace cg
{
	// actor acts as a container for components, if an object doesn't have component
	// do not define it as an actor
	class Component;

	class Actor : public GameObject, public ILifeCycleEvents
	{
	public:
		Actor() : cg::ILifeCycleEvents()
		{
		}
		template<typename T>
		std::shared_ptr<T> AddComponent(std::shared_ptr<cg::Component> component);
		bool RemoveComponentByName(const std::string& component_name);
		bool RemoveComponentByUUID(const std::string& uuid);
		std::shared_ptr<cg::Component> GetComponentByUUID(const std::string& uuid) const;
		std::shared_ptr<cg::Component> GetComponentByName(const std::string& name) const;
		template<typename T>
		std::shared_ptr<T> GetComponentByClass() const;
		template<typename T>
		std::vector<std::shared_ptr<T>> GetComponentsByClass() const;

		void Begin() override {}
		void Update(float delta) override {}

	protected:
		// TODO: consider putting a restriction to derived class on what can be added to components
		// this can be done by defining a pure virtual function conducting type check and calling it before trying
		// to find any component
		std::vector<std::shared_ptr<cg::Component>> components;
	};

	template<typename T>
	std::shared_ptr<T>
		cg::Actor::AddComponent(std::shared_ptr<cg::Component> component)
	{
		this->components.push_back(component);
		return std::dynamic_pointer_cast<T>(component);
	}

	template<typename T>
	std::vector<std::shared_ptr<T>>
		cg::Actor::GetComponentsByClass() const
	{
		std::vector<std::shared_ptr<T>> results;
		for (const auto& comp : this->components)
		{
			std::shared_ptr<T> t = std::dynamic_pointer_cast<T>(comp);
			if (t != nullptr)
			{
				results.push_back(t);
			}
		}
		return results;
	}

	template<typename T>
	std::shared_ptr<T>
		cg::Actor::GetComponentByClass() const
	{
		for (const auto& comp : this->components)
		{
			std::shared_ptr<T> t = std::dynamic_pointer_cast<T>(comp);
			if (t != nullptr)
			{
				return t;
			}
		}
	}

}
#endif // CG_ACTOR_H