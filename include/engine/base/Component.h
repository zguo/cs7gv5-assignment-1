#pragma once
#ifndef CG_COMPONENT_BASE_H
#define CG_COMPONENT_BASE_H

#include "common.h"

#include "GameObject.h"

namespace cg
{
	class Component : public GameObject
	{
	public:
	protected:
	private:
	};
}

#endif // CG_COMPONENT_BASE_H