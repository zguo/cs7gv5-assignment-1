#ifndef CG_GAME_OBJECT_H
#define CG_GAME_OBJECT_H

#include "common.h"

#include "ObjectBase.h"

namespace cg
{
	class GameObject : public ObjectBase
	{
	public:
		GameObject() = default;
		virtual ~GameObject() = default;

		template <typename T>
		bool IsDerivedFrom();

		std::string GetName() const;
		void SetName(const std::string& name);

		virtual void Update(float delta);

	protected:
		std::string name;
	};

	template <typename T>
	bool GameObject::IsDerivedFrom()
	{
		if (dynamic_cast<T*>(this))
			return true;
		else
			return false;
	}

}

#endif