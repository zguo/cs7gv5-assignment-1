#ifndef CG_OBJECT_BASE_H
#define CG_OBJECT_BASE_H

#include "common.h"
#include <objbase.h>

namespace cg
{
	class ObjectBase
	{
	public:
		ObjectBase();
		std::string GetUUID() const;

	protected:
		std::string uuid;
	};
}

#endif // CG_OBJECT_BASE_H