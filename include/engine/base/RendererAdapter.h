#ifndef CG_RENDERER_ADAPTER_H
#define CG_RENDERER_ADAPTER_H

#include "common.h"
#include "GameObject.h"

namespace cg
{
	class RendererAdapter :public GameObject
	{
	public:
		RendererAdapter();
		virtual ~RendererAdapter();

		virtual void Use() const = 0;
		virtual void SetFloat(const std::string& name, GLfloat value) = 0;
		virtual void SetInt(const std::string& name, GLint value) = 0;
		virtual void SetMat4(const std::string& name, glm::mat4 value) = 0;
		virtual void SetVec3(const std::string& name, glm::vec3 value) = 0;
	};
}

#endif // CG_RENDERER_ADAPTER_H