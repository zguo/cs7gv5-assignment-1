#ifndef CG_COMMON_H
#define CG_COMMON_H

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

#include "glad/glad.h"
#include "GLFW/glfw3.h"

#include "imgui/imgui.h"
#include "imgui/imgui_impl_glfw.h"
#include "imgui/imgui_impl_opengl3.h"

#include <memory>
#include <vector>
#include <map>
#include <iostream>
#include <string>
#include <algorithm>

#endif	// CG_COMMON_H