#ifndef CG_BOUNDING_BOX_H
#define CG_BOUNDING_BOX_H

#include "common.h"

namespace cg
{
    enum class BB_POS
    {
        LEFT_BOTTOM_FRONT, LEFT_BOTTOM_BACK,
        LEFT_TOP_FRONT, LEFT_TOP_BACK,
        RIGHT_BOTTOM_FRONT, RIGHT_BOTTOM_BACK,
        RIGHT_TOP_FRONT, RIGHT_TOP_BACK,
    };

    class BoundingBox
    {
    public:
        BoundingBox()
        {
            this->coordinates.reserve(8);
        }
        std::vector<glm::vec3> coordinates;
        float min_x;
        float max_x;
        float min_y;
        float max_y;
        float min_z;
        float max_z;
    };
}

#endif // CG_BOUNDING_BOX_H