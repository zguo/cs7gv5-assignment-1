#pragma once
#ifndef CG_C_CAMERA_H
#define CG_C_CAMERA_H

#include "common.h"

#include "Component.h"
#include "CTransform.h"

namespace cg
{
	class CCamera :public Component
	{
	public:
		CCamera() = default;
		CCamera(glm::vec3 local_position, glm::vec3 local_rotation);
		glm::vec3 GetLocalPosition() const;
		void SetLocalPosition(glm::vec3 position);
		glm::vec3 GetLocalRotation() const;
		void SetLocalRotation(glm::vec3 rotation);
	private:
		std::shared_ptr<cg::CTransform> transform_comp;
		glm::vec3 offset;
	};
}

#endif // CG_C_CAMERA_H
