#pragma once
#ifndef CG_SKELETAL_MESH_COMPONENT_H
#define CG_SKELETAL_MESH_COMPONENT_H

#include "common.h"

#include "assimp/Importer.hpp"
#include "assimp/scene.h"
#include "assimp/postprocess.h"

#include "SDynamicMesh.h"
#include "STexture.h"
#include "Component.h"
#include "CBoundingBox.h"
#include "ITransformable.h"
#include "Animation.h"

namespace cg
{
	class DynamicMeshComponent : public Component, public ITransformable
	{
	public:
		DynamicMeshComponent() = default;
		DynamicMeshComponent(const std::string& path);

		std::vector<std::shared_ptr<cg::DynamicMesh>> GetMeshes() const { return this->meshes; }
		std::shared_ptr<cg::BoundingBox> GetBoundingBox() const { return this->bounding_box; }

		const aiScene* GetScene() { return this->scene; }
		const std::vector<cg::Texture> GetTextures() const { return this->textures; }

		void SetAnimation(std::shared_ptr<cg::Animation> animation) { this->animation = animation; }
		void EnableAnimation() { this->b_enable_animation = true; }
		void DisableAnimation() { this->b_enable_animation = false; }
		bool IsAnimationEnabled() const { return this->b_enable_animation; }
		void PlayAnimation(float delta_time);

	private:
		std::vector<std::shared_ptr<cg::DynamicMesh>> meshes;

		// bone-related
		Assimp::Importer importer;
		const aiScene* scene;
		std::vector<cg::Texture> textures;

		// bounding box
		std::shared_ptr<cg::BoundingBox> bounding_box;
		float max_x = 0.0f;
		float min_x = 0.0f;
		float max_y = 0.0f;
		float min_y = 0.0f;
		float max_z = 0.0f;
		float min_z = 0.0f;

		// animation
		std::shared_ptr<cg::Animation> animation;
		bool b_enable_animation = false;

		void LoadModel(const std::string& path);
		void ProcessNode(aiNode* node, const aiScene* scene);
		std::shared_ptr<cg::DynamicMesh> ProcessMesh(aiMesh* mesh, const aiScene* scene);
		std::vector<cg::Texture> LoadTextures(const aiScene* scene, aiMaterial* mat, aiTextureType type, cg::ETextureType cg_type);
	};
}

#endif // CG_SKELETAL_MESH_COMPONENT_H
