#pragma once
#ifndef CG_STATIC_MESH_COMPONENT_H
#define CG_STATIC_MESH_COMPONENT_H

#include "common.h"

#include "assimp/Importer.hpp"
#include "assimp/scene.h"
#include "assimp/postprocess.h"

#include "SStaticMesh.h"
#include "STexture.h"
#include "Component.h"
#include "CBoundingBox.h"
#include "ITransformable.h"

namespace cg
{
	class StaticMeshComponent : public Component, public ITransformable
	{
	public:
		StaticMeshComponent() = default;
		StaticMeshComponent(const std::string& path);
		std::vector<std::shared_ptr<cg::StaticMesh>> GetMeshes() const { return this->meshes; }
		std::shared_ptr<cg::BoundingBox> GetBoundingBox() const { return this->bounding_box; }

	private:
		std::vector<std::shared_ptr<cg::StaticMesh>> meshes;
		std::vector<cg::Texture> textures;
		
		// bounding box
		std::shared_ptr<cg::BoundingBox> bounding_box;
		
		void LoadModel(const std::string& path);
		void ProcessNode(aiNode* node, const aiScene* scene);
		std::shared_ptr<cg::StaticMesh> ProcessMesh(aiMesh* mesh, const aiScene* scene);
		std::vector<cg::Texture> LoadTextures(const aiScene* scene, aiMaterial* mat, aiTextureType type, cg::ETextureType cg_type);
	};
}

#endif // CG_STATIC_MESH_COMPONENT_H
