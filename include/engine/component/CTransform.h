#pragma once
#ifndef CG_C_TRANSFORM_H
#define CG_C_TRANSFORM_H

#include "common.h"

#include "Component.h"

namespace cg
{
	class CTransform : public Component
	{
	public:
		CTransform();
		glm::vec3 GetLocalPosition() const;
		void SetLocalPosition(glm::vec3 val);
		void SetLocalPosition(float x, float y, float z);

		glm::vec3 GetGlobalPosition() const;
		void SetGlobalPosition(glm::vec3 val);
		void SetGlobalPosition(float x, float y, float z);

		glm::vec3 GetLocalRotation() const;
		void SetLocalRotation(glm::vec3 val);
		void SetLocalRotation(float x, float y, float z);
		void RotateAround(float degree, glm::vec3 axis);

		glm::vec3 GetGlobalRotation() const;
		void SetGlobalRotation(glm::vec3 val);
		void SetGlobalRotation(float x, float y, float z);

		glm::vec3 GetLocalScale() const;
		void SetLocalScale(glm::vec3 val);
		void SetLocalScale(float x, float y, float z);

		glm::vec3 GetGlobalScale() const;
		void SetGlobalScale(glm::vec3 val);
		void SetGlobalScale(float x, float y, float z);

	private:
		glm::vec3 local_position;
		glm::vec3 global_position;
		glm::vec3 local_rotation;
		glm::vec3 global_rotation;
		glm::vec3 local_scale;
		glm::vec3 global_scale;
	};

}

#endif // CG_C_TRANSFORM_H
