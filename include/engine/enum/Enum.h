#pragma once
#ifndef CG_ENUM_H
#define CG_ENUM_H

#include "common.h"

namespace cg
{
	enum class LogLevel
	{
        DEFAULT,
        ERR,
        WARNING,
        INFO
	};

    enum class ELightType
    {
        NONE,
        POINT,
        DIRECTIONAL,
        SPOTLIGHT,
        AMBIENT
    };

    enum class EPlayerMovement
    {
        FORWARD,
        BACKWARD,
        LEFT,
        RIGHT,
        UP,
        DOWN
    };

    enum class ECameraMode
    {
        FIRST_PERSON,
        THIRD_PERSON,
        FREE,
        FIXED,
        OVERHEAD
    };

    enum class EShaderType
    {
        VERTEX,
        FRAGMENT
    };

    enum class ETextureType
    {
        NONE,
        DIFFUSE_MAP,
        NORMAL_MAP,
        SPECULAR_MAP
    };

    enum class EShaderTarget
    {
        MODEL,
        LIGHT
    };
}

#endif // CG_ENUM_H