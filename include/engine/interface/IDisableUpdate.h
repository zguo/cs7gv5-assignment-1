#ifndef CG_I_DISABLE_UPDATE_H
#define CG_I_DISABLE_UPDATE_H

#include "common.h"
#include "IPrimitive.h"

namespace cg
{
	class IDisableUpdate :public IPrimitive
	{
	public:
		IDisableUpdate() = default;
		virtual ~IDisableUpdate() = default;
	};
}

#endif // CG_I_DISABLE_UPDATE_H