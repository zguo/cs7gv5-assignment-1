#pragma once
#ifndef CG_I_LIFE_CYCLE_EVENTS_H
#define CG_I_LIFE_CYCLE_EVENTS_H

#include "common.h"

#include "IPrimitive.h"

namespace cg
{
	class ILifeCycleEvents :public IPrimitive
	{
		friend class GameInstance;
	public:
		ILifeCycleEvents()
		{
			ILifeCycleEvents::update_list.push_back(this);
		}
		virtual ~ILifeCycleEvents() = default;
		virtual void Begin() = 0;
		virtual void Update(float delta) = 0;
	protected:
	private:
		static void Destroy(ILifeCycleEvents*);
		static std::vector<ILifeCycleEvents*> update_list;
	};
}

#endif // CG_I_LIFE_CYCLE_EVENTS_H

