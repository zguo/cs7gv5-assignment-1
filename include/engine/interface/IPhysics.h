#ifndef CG_I_PHYSICS_H
#define CG_I_PHYSICS_H

#include "common.h"

#include "CBoundingBox.h"
#include "IPrimitive.h"

namespace cg
{
	class IPhysics :public IPrimitive
	{
	public:
		IPhysics()
		{
			this->bounding_box = std::make_shared<cg::BoundingBox>();
			cg::IPhysics::ALL_BOUNDING_BOXES.push_back(this->bounding_box);
		}
		virtual ~IPhysics() = default;

		bool IsPhysicsEnabled() const { return this->b_enable_physics; }
		bool IsGravityEnabled() const { return this->b_enable_gravity; }

		bool CheckCollision(cg::BoundingBox* another_bb)
		{
			return ((bounding_box->min_x >= another_bb->min_x && bounding_box->min_x <= another_bb->max_x) || (another_bb->min_x >= bounding_box->min_x && another_bb->min_x <= bounding_box->max_x)) &&
				((bounding_box->min_y >= another_bb->min_y && bounding_box->min_y <= another_bb->max_y) || (another_bb->min_y >= bounding_box->min_y && another_bb->min_y <= bounding_box->max_y)) &&
				((bounding_box->min_z >= another_bb->min_z && bounding_box->min_z <= another_bb->max_z) || (another_bb->min_z >= bounding_box->min_z && another_bb->min_z <= bounding_box->max_z));
		}

		virtual void ProcessGravity() {}

		static void AddBoundingBox(std::shared_ptr<cg::BoundingBox> bb);

	protected:
		bool b_enable_physics;
		bool b_enable_gravity;
		bool b_enable_trigger;
		std::shared_ptr<cg::BoundingBox> bounding_box;

	private:
		static std::vector<std::shared_ptr<cg::BoundingBox>> ALL_BOUNDING_BOXES;
	};
}

#endif // CG_I_PHYSICS_H