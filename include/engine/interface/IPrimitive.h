#ifndef CG_I_PRIMITIVE_H
#define CG_I_PRIMITIVE_H

#include "common.h"

namespace cg
{
	class IPrimitive
	{
	public:
		IPrimitive() = default;
		virtual ~IPrimitive() = default;

		template <typename T>
		bool IsDerivedFrom();
	};

	template <typename T>
	bool cg::IPrimitive::IsDerivedFrom()
	{
		if (dynamic_cast<T*>(this))
			return true;
		else
			return false;
	}

}

#endif // CG_I_PRIMITIVE_H
