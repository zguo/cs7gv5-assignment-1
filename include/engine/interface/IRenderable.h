#pragma once
#ifndef CG_I_RENDERABLE_H
#define CG_I_RENDERABLE_H

#include "common.h"

#include "Renderer.h"
#include "IPrimitive.h"

namespace cg
{
	class IRenderable :public IPrimitive
	{
	public:
		IRenderable() : renderer(std::make_shared<cg::Renderer>()) {}
		virtual void Render(float delta_time) = 0;
		std::shared_ptr<cg::Renderer> GetRenderer() const { return this->renderer; }

	protected:
		std::shared_ptr<cg::Renderer> renderer;
	};
}

#endif // CG_I_RENDERABLE_H