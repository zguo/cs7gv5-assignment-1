#pragma once
#ifndef CG_I_TRANSFORMABLE_H
#define CG_I_TRANSFORMABLE_H

#include "common.h"

#include "CTransform.h"
#include "IPrimitive.h"

namespace cg
{
	class ITransformable :public IPrimitive
	{
	public:
		ITransformable() : transform_comp(std::make_shared<CTransform>()) {}

		glm::vec3 GetLocalPosition() const { return this->transform_comp->GetLocalPosition(); }
		void SetLocalPosition(glm::vec3 val) { this->transform_comp->SetLocalPosition(val); }
		void SetLocalPosition(float x, float y, float z) { this->transform_comp->SetLocalPosition(x, y, z); }

		glm::vec3 GetGlobalPosition() const { return this->transform_comp->GetGlobalPosition(); }
		void SetGlobalPosition(glm::vec3 val) { this->transform_comp->SetGlobalPosition(val); }
		void SetGlobalPosition(float x, float y, float z) { this->transform_comp->SetGlobalPosition(x, y, z); }

		glm::vec3 GetLocalRotation() const { return this->transform_comp->GetLocalRotation(); }
		void SetLocalRotation(glm::vec3 val) { this->transform_comp->SetLocalRotation(val); }
		void SetLocalRotation(float x, float y, float z) { this->transform_comp->SetLocalRotation(x, y, z); }
		void RotateAround(float degree, glm::vec3 axis) { this->transform_comp->RotateAround(degree, axis); }

		glm::vec3 GetGlobalRotation() const { return this->transform_comp->GetGlobalRotation(); }
		void SetGlobalRotation(glm::vec3 val) { this->transform_comp->SetGlobalRotation(val); }
		void SetGlobalRotation(float x, float y, float z) { this->transform_comp->SetGlobalRotation(x, y, z); }

		glm::vec3 GetLocalScale() const { return this->transform_comp->GetLocalScale(); }
		void SetLocalScale(glm::vec3 val) { this->transform_comp->SetLocalScale(val); }
		void SetLocalScale(float x, float y, float z) { this->transform_comp->SetLocalScale(x, y, z); }

		glm::vec3 GetGlobalScale() const { return this->transform_comp->GetGlobalScale(); }
		void SetGlobalScale(glm::vec3 val) { this->transform_comp->SetGlobalScale(val); }
		void SetGlobalScale(float x, float y, float z) { this->transform_comp->SetGlobalScale(x, y, z); }

		virtual ~ITransformable() = default;

	protected:
		std::shared_ptr<cg::CTransform> transform_comp;
	};
}

#endif // CG_I_TRANSFORMABLE_H
