#ifndef CG_OPENGL_ADAPTER_H
#define CG_OPENGL_ADAPTER_H

#include "common.h"

#include "RendererAdapter.h"
#include "Logger.h"

namespace cg
{
	class OpenGLAdapter final : public RendererAdapter
	{
	public:
		OpenGLAdapter();
		OpenGLAdapter(const std::string& vertex_shader_code, const std::string& fragment_shader_code);

		void SetFloat(const std::string& name, GLfloat value) override;
		void SetInt(const std::string& name, GLint value) override;
		void SetMat4(const std::string& name, glm::mat4 value) override;
		void SetVec3(const std::string& name, glm::vec3 value) override;
		void Use() const override;

		~OpenGLAdapter();

	private:
		GLuint program_id;
	};
}

#endif // CG_OPENGL_ADAPTER_H