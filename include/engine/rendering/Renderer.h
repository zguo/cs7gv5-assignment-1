#pragma once
#ifndef CG_RENDERER_H
#define CG_RENDERER_H

#include "common.h"

#include "GameObject.h"
#include "RendererAdapter.h"

namespace cg
{
	class Renderer : public GameObject
	{
	public:
		Renderer();
		Renderer(const std::string& vertex_shader_code, const std::string& fragment_shader_code);
		void ActivateContext() const;
		void SetFloat(const std::string& name, GLfloat value) const;
		void SetInt(const std::string& name, GLint value) const;
		void SetMat4(const std::string& name, glm::mat4 value) const;
		void SetVec3(const std::string& name, glm::vec3 value) const;

	protected:
		std::shared_ptr<cg::RendererAdapter> adapter;
	};
}

#endif // CG_RENDERER_H
