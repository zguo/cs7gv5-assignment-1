#ifndef CG_S_DYNAMIC_MESH_H
#define CG_S_DYNAMIC_MESH_H

#include "common.h"

#include "SVertex.h"
#include "STexture.h"
#include "SDynamicMeshVertex.h"
#include "Renderer.h"
#include "Utility.h"

namespace cg
{
	struct DynamicMesh
	{
	public:
		DynamicMesh() = default;
		DynamicMesh(
			const std::vector<DynamicMeshVertex>& vertices,
			const std::vector<GLuint>& indices,
			const std::vector<cg::Texture>& textures
		);
		void Draw(const cg::Renderer* renderer);
		GLuint GetVAO() const { return this->vao; }
		GLuint GetVBO() const { return this->vbo; }
		GLuint GetEBO() const { return this->ebo; }

		void AddTexture(Texture texture) { this->textures.push_back(texture); }

		GLuint vao, vbo, ebo;
		std::vector<DynamicMeshVertex> vertices;
		std::vector<GLuint> indices;
		std::vector<cg::Texture> textures;

	private:
	};
}

#endif // CG_S_DYNAMIC_MESH_H