#ifndef CG_S_DYNAMIC_MESH_VERTEX_H
#define CG_S_DYNAMIC_MESH_VERTEX_H

namespace cg
{
	struct DynamicMeshVertex
	{
		glm::vec3 position;
		glm::vec3 normal;
		glm::vec2 texture_coord;

		DynamicMeshVertex() :
			position(glm::vec3(0.0f)),
			normal(glm::vec3(0.0f)),
			texture_coord(glm::vec2(0.0f))
		{
		}

		DynamicMeshVertex(
			glm::vec3 position,
			glm::vec3 normal,
			glm::vec2 texture_coord
		) :
			position(position),
			normal(normal),
			texture_coord(texture_coord)
		{
		}
	};
}

#endif // CG_S_DYNAMIC_MESH_VERTEX_H