#pragma once
#ifndef CG_S_MATERIAL_H
#define CG_S_MATERIAL_H

#include "common.h"

#include "STexture.h"

namespace cg
{
	struct Material
	{
		std::shared_ptr<cg::Texture> diffuse;
		std::shared_ptr<cg::Texture> specular;
		float metallic;
		float subsurface;
		float roughness;
		float specularTint;
		float anisotropic;
		float sheen;
		float sheenTint;
		float clearcoat;
		float clearcoatGloss;

		// TODO: maps should have default templates
		Material() = default;
		Material(
			std::shared_ptr<cg::Texture> diffuse,
			std::shared_ptr<cg::Texture> specular,
			float metallic = 0.0f,
			float subsurface = 0.0f,
			float roughness = 0.5f,
			float specularTint = 0.0f,
			float anisotropic = 0.0f,
			float sheen = 0.0f,
			float sheenTint = 0.5f,
			float clearcoat = 0.0f,
			float clearcoatGloss = 1.0f
		) :
			diffuse(diffuse),
			specular(specular),
			metallic(metallic),
			subsurface(subsurface),
			roughness(roughness),
			specularTint(specularTint),
			anisotropic(anisotropic),
			sheen(sheen),
			sheenTint(sheenTint),
			clearcoat(clearcoat),
			clearcoatGloss(clearcoatGloss)
		{}
	};
}

#endif // CG_S_MATERIAL_H
