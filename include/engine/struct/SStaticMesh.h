#ifndef CG_S_STATIC_MESH_H
#define CG_S_STATIC_MESH_H

#include "common.h"

#include "SVertex.h"
#include "STexture.h"
#include "Renderer.h"

namespace cg
{
	struct StaticMesh
	{
	public:
		StaticMesh() = default;
		StaticMesh(
			const std::vector<cg::Vertex>& vertices,
			const std::vector<GLuint>& indices,
			const std::vector<cg::Texture>& textures
		);
		void Draw(const cg::Renderer* renderer);
		GLuint GetVAO() const { return this->vao; }
		GLuint GetVBO() const { return this->vbo; }
		GLuint GetEBO() const { return this->ebo; }
		void AddTexture(cg::Texture texture) { this->textures.push_back(texture); }

	private:
		GLuint vao, vbo, ebo;
		std::vector<cg::Vertex> vertices;
		std::vector<GLuint> indices;
		std::vector<cg::Texture> textures;
	};
}

#endif // CG_S_STATIC_MESH_H
