#pragma once
#ifndef CG_S_TEXTURE_H
#define CG_S_TEXTURE_H

#include "common.h"

#include "Enum.h"

namespace cg
{
	struct Texture
	{
		cg::ETextureType type;
		std::string path;
		GLuint id;
		Texture() = default;
		Texture(const std::string& texture_file_path, GLuint id, cg::ETextureType type) :
			type(type), path(texture_file_path), id(id)
		{}
	};
}

#endif // CG_S_TEXTURE_H
