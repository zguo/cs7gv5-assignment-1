#ifndef CG_S_VERTEX_H
#define CG_S_VERTEX_H

#include "common.h"

namespace cg
{
	struct Vertex
	{
	public:
		glm::vec3 position;
		glm::vec3 normal;
		glm::vec2 texture_coord;
		Vertex(glm::vec3 position, glm::vec2 texture_coord, glm::vec3 normal) :
			position(position), texture_coord(texture_coord), normal(normal)
		{}
		static int GetStride()
		{
			return sizeof(glm::vec3) + sizeof(glm::vec2) + sizeof(glm::vec3);
		}
		static std::vector<Vertex> ConstructFromArray(std::vector<GLfloat>& data)
		{
			std::vector<Vertex> results;
			int stride = Vertex::GetStride();
			int size = stride / sizeof(GLfloat);
			for (auto i = 0; i < data.size(); i += size)
			{
				results.push_back(Vertex(data.begin() + i));
			}
			return results;
		}
	private:
		Vertex(std::vector<GLfloat>::iterator begin) :
			position(*begin, *(begin + 1), *(begin + 2)),
			normal(*(begin + 3), *(begin + 4), *(begin + 5)),
			texture_coord(*(begin + 6), *(begin + 7))
		{}
	};
}

#endif // CG_S_VERTEX_H
