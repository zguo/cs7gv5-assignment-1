#pragma once
#ifndef CG_S_VERTEX_BONE_DATA_H
#define CG_S_VERTEX_BONE_DATA_H

#include "common.h"

namespace cg
{
	struct VertexBoneData
	{
		unsigned int id;
		float weight;
		VertexBoneData(unsigned int id, float weight) :id(id), weight(weight) {}
	};

}

#endif // CG_S_VERTEX_BONE_DATA_H