#ifndef CG_ANIMATION_H
#define CG_ANIMATION_H

#include "common.h"

namespace cg
{
	class DynamicMeshComponent;

	enum class AnimationPreset
	{
		SIMPLE_ROTATION
	};

	class Animation
	{
	public:
		Animation() = default;
		// simple rotation
		Animation(std::shared_ptr<cg::DynamicMeshComponent> mesh_comp, glm::vec3 axis, float speed, bool direction, bool persistent, int duration_in_second = 0);

		void Play(float delta_time);

	private:
		// common settings
		std::weak_ptr<cg::DynamicMeshComponent> mesh_comp;
		float time_elapsed;
		float total_time;
		cg::AnimationPreset preset;
		
		// simple rotation settings
		float speed;
		bool direction;
		bool persistent;
		glm::vec3 axis;
		int duration;
		void PlaySimpleRotation(float delta_time);
	};
}

#endif // CG_ANIMATION_H