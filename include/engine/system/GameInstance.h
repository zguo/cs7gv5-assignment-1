#ifndef CG_GAME_INSTANCE_H
#define CG_GAME_INSTANCE_H

#include "common.h"

#include "Scene.h"
#include "PlayerController.h"
#include "WindowManager.h"
#include "RendererManager.h"
#include "UIManager.h"
#include "SceneManager.h"
#include "GameObject.h"

const GLuint SCR_WIDTH = 1920;
const GLuint SCR_HEIGHT = 1080;

namespace cg
{
	class GameInstance : public GameObject
	{
	public:
		static std::shared_ptr<GameInstance> GetInstance();
		
		std::shared_ptr<PlayerController> GetPlayerController() const;
		std::shared_ptr<WindowManager> GetWindowManager() const;
		std::shared_ptr<RendererManager> GetRendererManager() const;
		std::shared_ptr<SceneManager> GetSceneManager() const;
		
		// 从GameInstance类静态容器中移除GameObject
		void RemoveObject(const GameObject* object);

		void Init();
		void Run();
		void Terminate();

	private:
		GameInstance() = default;
		GameInstance(const cg::GameInstance&) = delete;
		GameInstance& operator=(const cg::GameInstance&) = delete;
		~GameInstance() = default;

		static std::shared_ptr<cg::GameInstance> instance;
		static void Destroy(cg::GameInstance * instance);

		// TODO: not in use
		static std::map<std::string, std::unique_ptr<cg::GameObject>> objects;
		
		void InvokeUpdate(float delta);
		
		std::shared_ptr<RendererManager> renderer_manager;
		std::shared_ptr<PlayerController> player_controller;
		std::shared_ptr<WindowManager> window_manager;
		std::shared_ptr<UIManager> ui_manager;
		std::shared_ptr<SceneManager> scene_manager;

		float delta_time = 0.0f;
		float last_frame = 0.0f;
	};
}

#endif // CG_GAME_INSTANCE_H