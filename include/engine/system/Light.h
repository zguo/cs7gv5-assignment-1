#pragma once
#ifndef CG_LIGHT_H
#define CG_LIGHT_H

#include "common.h"

#include "SceneActor.h"
#include "Enum.h"

namespace cg
{
	class Light : public SceneActor
	{
	public:
		Light() :
			diffuse(glm::vec3(1.0f)),
			type(cg::ELightType::NONE),
			b_calc_in_view_space(true)
		{}
		Light(cg::ELightType type, glm::vec3 colour = glm::vec3(1.0f)) :
			diffuse(colour),
			type(type),
			b_calc_in_view_space(true)
		{}
		// TODO: does it make sense to set scale for lights?
		float GetConstant() const { return this->constant; }
		void SetConstant(float value) { this->constant = value; }
		float GetLinear() const { return this->linear; }
		void SetLinear(float value) { this->linear = value; }
		float GetQuadratic() const { return this->quadratic; }
		void SetQuadratic(float value) { this->quadratic = value; }
		glm::vec3 GetAmbient() const { return this->ambient; }
		void SetAmbient(glm::vec3 value) { this->ambient = value; }
		glm::vec3 GetDiffuse() const { return this->diffuse; }
		void SetDiffuse(glm::vec3 value) { this->diffuse = value; }
		glm::vec3 GetSpecular() const { return this->specular; }
		void SetSpecular(glm::vec3 value) { this->specular = value; }
		void SetView(glm::mat4 value) { this->view = value; }
		void Render(float delta_time) override;
	private:
		cg::ELightType type;
		// uniforms
		glm::vec3 ambient;
		glm::vec3 diffuse;
		glm::vec3 specular;
		float constant;
		float linear;
		float quadratic;
		glm::mat4 view;
		bool b_calc_in_view_space;
	};

}

#endif // CG_LIGHT_H
