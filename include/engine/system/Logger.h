#pragma once
#ifndef CG_LOGGER_H
#define CG_LOGGER_H

#include "common.h"

#include "Enum.h"

namespace cg
{
	class Logger
	{
	public:
		static void Log(const std::string& log, cg::LogLevel level = cg::LogLevel::DEFAULT);
		static void Log(const glm::vec3 vec, cg::LogLevel level = cg::LogLevel::DEFAULT);
	private:
	};
}

#endif // CG_LOGGER_H
