#pragma once
#ifndef CG_PLAYER_CONTROLLER_H
#define CG_PLAYER_CONTROLLER_H

#include "common.h"

#include "GameObject.h"
#include "ACamera.h"
#include "Player.h"
#include "ILifeCycleEvents.h"
#include "Light.h"
#include "Logger.h"

namespace cg
{
	class PlayerController : public GameObject, public ILifeCycleEvents
	{
	public:
		PlayerController();
		std::shared_ptr<cg::Camera> GetCurrentCamera() const { return this->current_camera; }
		void SetCurrentCamera(std::shared_ptr<cg::Camera> camera) { this->current_camera = camera; }
		std::shared_ptr<cg::Camera> GetCameraNext();

		std::shared_ptr<cg::Player> GetPlayer() const { return this->player; }
		std::shared_ptr<cg::Player> AddPlayer(std::shared_ptr<cg::Player> player);

		// moving light
		void AddLight(std::shared_ptr<cg::Light> light) { this->light = light; }
		std::shared_ptr<cg::Light> GetLight() const { return this->light; }

		// game cycle
		void GainScore(int score)
		{
			this->score += score;
			if (this->score < 0)
				this->DisplayGameOver();
		}
		void DisplayGameOver()
		{
			cg::Logger::Log("Game Over !");
		}

		// callbacks
		void (*CursorPosCallback)(cg::PlayerController *self, GLFWwindow *window, double x_pos, double y_pos);
		void (*ScrollCallback)(cg::PlayerController *self, GLFWwindow *window, double x_offset, double y_offset);
		void (*SetKeyCallback)(cg::PlayerController *self, GLFWwindow *window, int key, int scancode, int action, int mods);
		void (*SetMouseButtonCallback)(cg::PlayerController *self, GLFWwindow *window, int button, int action, int mods);

		void Begin() override {}
		void Update(float delta) override;

	private:
		void MoveTowards(cg::EPlayerMovement direction, float delta_time) const;
		std::shared_ptr<cg::Camera> current_camera;
		std::shared_ptr<cg::Player> player;
		float walk_speed = 5.0f;

		int score = 0;

		void UpdatePlayerRotation()
		{
		}

		// moving light
		std::shared_ptr<cg::Light> light;

		void ProcessJump(float delta_time);
	};
}

#endif // CG_PLAYER_CONTROLLER_H