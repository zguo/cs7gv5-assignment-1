#ifndef CG_RENDERER_MANAGER_H
#define CG_RENDERER_MANAGER_H

#include "common.h"

#include "Renderer.h"
#include "GameObject.h"
#include "Enum.h"

namespace cg
{
	class RendererManager :public GameObject
	{
	public:
		RendererManager();
		~RendererManager() = default;
		std::shared_ptr<cg::Renderer> GetRendererByName(const std::string& name) const;
		std::shared_ptr<cg::Renderer> AddRenderer(const std::shared_ptr<cg::Renderer> renderer);
		std::shared_ptr<cg::Renderer> GetDefaultRenderer() const;
		std::shared_ptr<cg::Renderer> GenerateRenderer(const std::string& vertex_shader_code, const std::string& fragment_shader_code);
		std::vector<std::shared_ptr<cg::Renderer>>& GetAllRenderers() { return this->renderers; }
	private:
		std::vector<std::shared_ptr<cg::Renderer>> renderers;
		std::string GenerateDefaultShaderCode(cg::EShaderTarget target, cg::EShaderType type);
	};
}

#endif // CG_RENDERER_MANAGER_H