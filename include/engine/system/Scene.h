#pragma once
#ifndef CG_SCENE_H
#define CG_SCENE_H

#include "common.h"

#include "GameObject.h"
#include "ACamera.h"
#include "CDynamicMesh.h"
#include "Light.h"
#include "SceneActor.h"
#include "ILifeCycleEvents.h"

namespace cg
{
	class Scene : public GameObject
	{
	public:
		Scene() = default;
		void RenderAll(float delta_time) const;
		template <typename T>
		std::shared_ptr<T> AddActor(const std::shared_ptr<cg::SceneActor> actor);
		std::shared_ptr<cg::Light> AddLight(const std::shared_ptr<cg::Light> light);
		std::shared_ptr<cg::Camera> GetCameraNext(const std::shared_ptr<cg::Camera> current_camera);
		std::shared_ptr<cg::Camera> GetCameraByName(const std::string &name);
		std::shared_ptr<cg::Camera> AddCamera(const std::shared_ptr<cg::Camera> camera);
		std::vector<std::shared_ptr<cg::SceneActor>>& GetActors() { return this->actors; }

	protected:
	private:
		std::vector<std::shared_ptr<cg::SceneActor>> actors;
		std::vector<std::shared_ptr<cg::Light>> lights;
		std::vector<std::shared_ptr<cg::Camera>> cameras;
	};

	template <typename T>
	std::shared_ptr<T>
	cg::Scene::AddActor(const std::shared_ptr<cg::SceneActor> actor)
	{
		this->actors.push_back(actor);
		return std::dynamic_pointer_cast<T>(actor);
	}
}

#endif // CG_SCENE_H