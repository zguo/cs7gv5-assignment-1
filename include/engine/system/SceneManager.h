#ifndef CG_SCENE_MANAGER_H
#define CG_SCENE_MANAGER_H

#include "common.h"

#include "Scene.h"

namespace cg
{
	class SceneManager : public GameObject
	{
	public:
		SceneManager() : current_scene(std::make_shared<cg::Scene>()) {}
		std::shared_ptr<cg::Scene> GetCurrentScene() const { return this->current_scene; }

	private:
		std::shared_ptr<Scene> current_scene;
	};
}

#endif	// CG_SCENE_MANAGER_H