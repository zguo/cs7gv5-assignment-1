#ifndef CG_SKY_BOX_H
#define CG_SKY_BOX_H

#include "common.h"

#include "stb/stb_image.h"

#include "IRenderable.h"
#include "GameObject.h"
#include "SceneActor.h"

namespace cg
{
	class SkyBox : public SceneActor
	{
	public:
		SkyBox() = default;
		SkyBox(const std::string& texture_folder, const std::string& vs_path, const std::string& fs_path);

		void Render(float delta_time) override;

	private:
		GLuint id;
		GLuint vao;
		GLuint vbo;
		std::vector<glm::vec3> vertices;
		std::vector<std::string> faces;

		unsigned int LoadCubemap(const std::vector<std::string>& faces);
	};
}

#endif // CG_SKY_BOX_H