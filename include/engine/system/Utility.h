#pragma once
#ifndef CG_UTILITY_H
#define CG_UTILITY_H

#include "common.h"
#include "assimp/Importer.hpp"
#include "assimp/scene.h"
#include "assimp/postprocess.h"
#include "stb/stb_image.h"

#include <iostream>
#include <fstream>
#include <sstream>

#include "ACamera.h"
#include "STexture.h"
#include "Enum.h"

namespace cg
{
	const std::string ROOT_DIR = R"(./)";
	const std::string RESOURCE_DIR = ROOT_DIR + "resource\\";
	const int MAX_BONE = 150;
	const int MAX_BONE_INFLUENCE = 4;
	class Utility
	{
	public:
		static void FramebufferSizeCallback(GLFWwindow* window, int width, int height);
		static void ProcessInput(GLFWwindow* window);
		// this function should only deal with moving and camera logic
		// all other key input should be handled in SetKeyCallback
		static void ProcessInputEveryFrame(GLFWwindow* window, double delta_time);
		static GLFWwindow* Init();
		static std::string LoadShaderCodeFromFile(const std::string file_name);
		static std::string GenerateDefaultShaderCode(cg::EShaderTarget target, cg::EShaderType type);
		static void SetInput(GLFWwindow* window);
		// for all other key inputs that do not require update every frame, use the following callback instead
		static void SetKeyCallbackWrapper(GLFWwindow* window, int key, int scancode, int action, int mods);
		static void CursorPosCallbackWrapper(GLFWwindow* window, double x_pos, double y_pos);
		static void ScrollCallbackWrapper(GLFWwindow* window, double x_offset, double y_offset);
		static void SetMouseButtonCallback(GLFWwindow* window, int button, int action, int mods);

		static glm::mat4 CalcModelMatrix(glm::vec3 rotate, glm::vec3 translate);
		static glm::mat4 CalcYaw(float yaw);
		static glm::mat4 CalcPitch(float pitch);
		static glm::mat4 CalcRoll(float roll);

		static inline glm::vec3 vec3_cast(const aiVector3D& v) { return glm::vec3(v.x, v.y, v.z); }
		static inline glm::vec2 vec2_cast(const aiVector3D& v) { return glm::vec2(v.x, v.y); }
		static inline glm::quat quat_cast(const aiQuaternion& q) { return glm::quat(q.w, q.x, q.y, q.z); }
		static inline glm::mat4 mat4_cast(const aiMatrix4x4& m) { return glm::transpose(glm::make_mat4(&m.a1)); }
		static inline glm::mat4 mat4_cast(const aiMatrix3x3& m) { return glm::transpose(glm::make_mat3(&m.a1)); }

		template<typename ... Args>
		static std::string Format(const std::string& format, Args ... args)
		{
			size_t size = snprintf(nullptr, 0, format.c_str(), args ...) + 1;
			if (size <= 0) { throw std::runtime_error("Error during formatting."); }
			std::unique_ptr<char[]> buf(new char[size]);
			snprintf(buf.get(), size, format.c_str(), args ...);
			return std::string(buf.get(), buf.get() + size - 1);
		}

		static GLuint LoadTextureFromFile(const std::string& path, GLint wrap_mode, GLint mag_filter_mode, GLint min_filter_mode, cg::ETextureType type);
		static GLuint LoadTextureEmbedded(const aiTexture* texture, GLint wrap_mode, GLint mag_filter_mode, GLint min_filter_mode, cg::ETextureType type);
		static cg::Texture LoadDefaultTexture();
	private:
		static int GetChannels(cg::ETextureType type);
	};
}

#endif // CG_UTILITY_H
