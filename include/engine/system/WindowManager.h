#pragma once
#ifndef CG_WINDOW_MANAGER_H
#define CG_WINDOW_MANAGER_H

#include "common.h"

#include "GameObject.h"

namespace cg
{
	class WindowManager : public GameObject
	{
	public:
		WindowManager(GLFWwindow* window, float width, float height) : width(width), height(height)
		{
			this->window = window;
		}
		float GetWidth() const { return this->width; }
		float GetHeight() const { return this->height; }
		void SetWidth(float value) { this->width = value; }
		void SetHeight(float value) { this->height = value; }
		GLFWwindow* GetWindow() const { return this->window; }
	private:
		float width;
		float height;
		GLFWwindow* window;
	};
}

#endif // CG_WINDOW_MANAGER_H