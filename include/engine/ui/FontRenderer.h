#ifndef CG_FONT_RENDERER_H
#define CG_FONT_RENDERER_H

#include "common.h"

#include "ft2build.h"
#include FT_FREETYPE_H

#include "IRenderable.h"

namespace cg
{
	struct Character
	{
		GLuint TextureID;   // 字形纹理的ID
		glm::ivec2 Size;    // 字形大小
		glm::ivec2 Bearing; // 从基准线到字形左部/顶部的偏移值
		GLuint Advance;     // 原点距下一个字形原点的距离
	};

	class FontRenderer : public IRenderable
	{
	public:
		FontRenderer();

		void Render(float delta) override;

		void SetParameters(const std::string& text, float pos_x, float pos_y, float scale, glm::vec3 color);

	private:
		GLuint vao;
		GLuint vbo;
		glm::vec3 color;
		std::string text;
		float pos_x;
		float pos_y;
		float scale;
		float window_width;
		float window_height;
		std::map<GLchar, Character> Characters;
	};
}

#endif // CG_FONT_RENDERER_H