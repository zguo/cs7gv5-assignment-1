#pragma once
#ifndef CG_PLAYER_H
#define CG_PLAYER_H

#include "common.h"

#include "Enum.h"
#include "ADynamicModel.h"
#include "SceneActor.h"

namespace cg
{
	class Player : public DynamicModel
	{
	public:
		Player(const std::string& model_file = "");

		void Update(float delta) override;

	private:
	};
}

#endif // CG_PLAYER_H