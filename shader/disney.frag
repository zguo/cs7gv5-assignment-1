#version 330 core

in vec2 TexCoord;
in vec3 Normal;
in vec3 FragPos;

out vec4 FragColour;

// uniform float metallic = 0.0;
// uniform float subsurface = 0.0;
// uniform float specular = 0.5;
// uniform float roughness = 0.5;
// uniform float specularTint = 0.0;
// uniform float anisotropic = 0.0;
// uniform float sheen = 0.0;
// uniform float sheenTint = 0.5;
// uniform float clearcoat = 0.0;
// uniform float clearcoatGloss = 1.0;

struct Material
{
	sampler2D diffuse;
	sampler2D specular;
    float metallic;
    float subsurface;
    float roughness;
    float specularTint;
    float anisotropic;
    float sheen;
    float sheenTint;
    float clearcoat;
    float clearcoatGloss;
};

struct Light
{
	vec3 position;
	vec3 direction;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
    float intensity;
};

uniform Material material;
uniform Light light;


const float PI = 3.14159265358979323846;

float sqr(float x) 
{
     return x * x; 
}

float SchlickFresnel(float u)
{
    float m = clamp(1-u, 0, 1);
    float m2 = m * m;
    return m2 * m2 * m;
}

// Berry distribution
float GTR1(float NdotH, float a)
{
    if (a >= 1) return 1/PI;
    float a2 = a*a;
    float t = 1 + (a2-1)*NdotH*NdotH;
    return (a2-1) / (PI*log(a2)*t);
}

// Trowbridge-Reitz distribution
float GTR2(float NdotH, float a)
{
    float a2 = a*a;
    float t = 1 + (a2-1)*NdotH*NdotH;
    return a2 / (PI * t*t);
}

float GTR2_aniso(float NdotH, float HdotX, float HdotY, float ax, float ay)
{
    return 1 / (PI * ax*ay * sqr( sqr(HdotX/ax) + sqr(HdotY/ay) + NdotH*NdotH ));
}

float smithG_GGX(float NdotV, float alphaG)
{
    float a = alphaG*alphaG;
    float b = NdotV*NdotV;
    return 1 / (NdotV + sqrt(a + b - a*b));
}

float smithG_GGX_aniso(float NdotV, float VdotX, float VdotY, float ax, float ay)
{
    return 1 / (NdotV + sqrt( sqr(VdotX*ax) + sqr(VdotY*ay) + sqr(NdotV) ));
}

vec3 mon2lin(vec3 x)
{
    return vec3(pow(x[0], 2.2), pow(x[1], 2.2), pow(x[2], 2.2));
}

vec3 BRDF(Material material, vec3 L/*Light*/, vec3 V/*View*/, vec3 N/*Normal*/, vec3 X/*Roughness along X axis*/, vec3 Y/*Roughness along Y axis*/)
{
    // uniforms defined in Material
    vec3 baseColour = vec3(texture(material.diffuse, TexCoord));
    float specular = float(texture(material.specular, TexCoord));
    float metallic = material.metallic;
    float subsurface = material.subsurface;
    float roughness = material.roughness;
    float specularTint = material.specularTint;
    float anisotropic = material.anisotropic;
    float sheen = material.sheen;
    float sheenTint = material.sheenTint;
    float clearcoat = material.clearcoat;
    float clearcoatGloss = material.clearcoatGloss;

    float NdotL = dot(N,L);
    float NdotV = dot(N,V);
    if (NdotL < 0 || NdotV < 0) return vec3(0);

    vec3 H = normalize(L+V);
    float NdotH = dot(N,H);
    float LdotH = dot(L,H);

    vec3 Cdlin = mon2lin(baseColour);
    float Cdlum = .3*Cdlin[0] + .6*Cdlin[1]  + .1*Cdlin[2]; // luminance approx.

    vec3 Ctint = Cdlum > 0 ? Cdlin/Cdlum : vec3(1); // normalize lum. to isolate hue+sat
    vec3 Cspec0 = mix(specular*.08*mix(vec3(1), Ctint, material.specularTint), Cdlin, material.metallic);
    vec3 Csheen = mix(vec3(1), Ctint, sheenTint);

    // Diffuse fresnel - go from 1 at normal incidence to .5 at grazing
    // and mix in diffuse retro-reflection based on roughness
    float FL = SchlickFresnel(NdotL), FV = SchlickFresnel(NdotV);
    float Fd90 = 0.5 + 2 * LdotH*LdotH * roughness;
    float Fd = mix(1.0, Fd90, FL) * mix(1.0, Fd90, FV);

    // Based on Hanrahan-Krueger brdf approximation of isotropic bssrdf
    // 1.25 scale is used to (roughly) preserve albedo
    // Fss90 used to "flatten" retroreflection based on roughness
    float Fss90 = LdotH*LdotH*roughness;
    float Fss = mix(1.0, Fss90, FL) * mix(1.0, Fss90, FV);
    float ss = 1.25 * (Fss * (1 / (NdotL + NdotV) - .5) + .5);

    // specular
    float aspect = sqrt(1-anisotropic*.9);
    float ax = max(.001, sqr(roughness)/aspect);
    float ay = max(.001, sqr(roughness)*aspect);
    float Ds = GTR2_aniso(NdotH, dot(H, X), dot(H, Y), ax, ay);
    float FH = SchlickFresnel(LdotH);
    vec3 Fs = mix(Cspec0, vec3(1), FH);
    float Gs;
    Gs  = smithG_GGX_aniso(NdotL, dot(L, X), dot(L, Y), ax, ay);
    Gs *= smithG_GGX_aniso(NdotV, dot(V, X), dot(V, Y), ax, ay);

    // sheen
    vec3 Fsheen = FH * sheen * Csheen;

    // clearcoat (ior = 1.5 -> F0 = 0.04)
    float Dr = GTR1(NdotH, mix(.1,.001,clearcoatGloss));
    float Fr = mix(.04, 1.0, FH);
    float Gr = smithG_GGX(NdotL, .25) * smithG_GGX(NdotV, .25);

    return ((1/PI) * mix(Fd, ss, subsurface) * Cdlin + Fsheen) * (1-metallic) + Gs * Fs * Ds + .25 * clearcoat * Gr * Fr * Dr, 1.0;
}

void main()
{
    // NOTE: light.direction should be transformed into view space before
    // sending to shaders
    vec3 lightDir = normalize(-light.direction);
    vec3 viewDir = vec3(0.0, 0.0, 0.0);
    vec3 normal = Normal;
    vec3 anisoX = vec3(1.0, 1.0, 1.0);
    vec3 anisoY = vec3(1.0, 1.0, 1.0);
    // float roughnessMap = texture(material.roughnessMap, TexCoord);
    FragColour =  vec4(clamp(light.intensity, 0.0, 1.0) * BRDF(material, lightDir, viewDir, normal, anisoX, anisoY), 1.0);
}