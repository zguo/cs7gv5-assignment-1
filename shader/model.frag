#version 330 core
out vec4 FragColor;

in vec2 TexCoord;
in vec3 Normal;
in vec3 FragPos;

struct Material
{
	sampler2D diffuse;
	sampler2D specular;
    float metallic;
    float subsurface;
    float roughness;
    float specularTint;
    float anisotropic;
    float sheen;
    float sheenTint;
    float clearcoat;
    float clearcoatGloss;
};

struct PointLight
{
	vec3 position;
	vec3 direction;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
    float intensity;
	float constant;
	float linear;
	float quadratic;
};

uniform Material material;
uniform PointLight light;

vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
	// diffuse
	vec3 lightDir = normalize(light.position - fragPos);
	// vec3 lightDir = normalize(-light.direction);
	float diff = max(dot(normal, lightDir), 0.0);
	vec3 texDiff = vec3(texture(material.diffuse, TexCoord));
	vec3 diffuse = light.diffuse * diff * texDiff;
	// ambient
	vec3 ambient = light.ambient * texDiff;
	// specular
	vec3 reflectDir = reflect(-lightDir, normal);
	float shininess = 32.0;
	float texSpec = float(texture(material.specular, TexCoord));
	vec3 specular = light.specular * (pow(max(dot(viewDir, reflectDir), 0.0), shininess) * texSpec);		// 32可以用uniform传进来，必须是2的幂
	// attenuation
	float dis = length(light.position - FragPos);
	float attenuation = 1.0 / (light.constant + light.linear * dis + light.quadratic * (dis * dis));
	return attenuation * (ambient + diffuse + specular);
}

void main()
{
	vec3 norm = normalize(Normal);
	vec3 viewDir = normalize(-FragPos);
	FragColor = vec4(CalcPointLight(light, norm, FragPos, viewDir), 1.0);
}