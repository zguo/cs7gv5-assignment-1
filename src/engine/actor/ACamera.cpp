#include "ACamera.h"
#include "Logger.h"

glm::mat4 cg::Camera::GetViewMatrix()
{
	switch (this->mode)
	{
	case cg::ECameraMode::FIRST_PERSON:
		return glm::lookAt(this->position, this->position + this->front, this->up);
		break;
	case cg::ECameraMode::THIRD_PERSON:
		return glm::lookAt(this->position, this->target, this->up);
		break;
	case cg::ECameraMode::FREE:
		return glm::lookAt(this->position, this->position + this->front, this->up);
		break;
	case cg::ECameraMode::FIXED:
		// TODO
		break;
	case cg::ECameraMode::OVERHEAD:
		return glm::lookAt(this->position, this->position + this->front, this->up);
		break;
	default:
		break;
	}
}

void cg::Camera::ProcessKeyboard(cg::EPlayerMovement direction, float delta_time)
{
	float velocity = this->movement_speed * delta_time;
	if (direction == cg::EPlayerMovement::FORWARD)
		this->position += this->front * velocity;
	if (direction == cg::EPlayerMovement::BACKWARD)
		this->position -= this->front * velocity;
	// for left and right, cross production needs to be normalised
	// otherwise its magnitude will depend on front vector and leads to uneven movement
	if (direction == cg::EPlayerMovement::LEFT)
		this->position -= glm::normalize(glm::cross(this->front, this->up)) * velocity;
	if (direction == cg::EPlayerMovement::RIGHT)
		this->position += glm::normalize(glm::cross(this->front, this->up)) * velocity;
	if (direction == cg::EPlayerMovement::UP)
		this->position += this->up * velocity;
	if (direction == cg::EPlayerMovement::DOWN)
		this->position -= this->up * velocity;
}