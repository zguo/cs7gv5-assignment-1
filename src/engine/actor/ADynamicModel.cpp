#include "ADynamicModel.h"
#include "GameInstance.h"
#include "Logger.h"

void cg::DynamicModel::Render(float delta_time)
{
	// update the model matrix
	this->renderer->ActivateContext();
	glm::mat4 global_transform(1.0f);
	/*global_transform = glm::translate(global_transform, this->GetGlobalPosition());*/
	for (const auto& mesh_comp : this->mesh_comps)
	{
		if (mesh_comp->IsAnimationEnabled())
			mesh_comp->PlayAnimation(delta_time);
		auto model = glm::mat4(1.0f);
		auto local_scale = mesh_comp->GetLocalScale();
		auto local_rot = mesh_comp->GetLocalRotation();
		auto local_pos = mesh_comp->GetLocalPosition();
		model = glm::scale(model, local_scale);
		//model = glm::mat4_cast(glm::normalize(glm::qua<GLfloat>(glm::radians(local_rot)))) * model;
		//model = this->rotation_matrix * model;
		if (DynamicModel::use_quaternion)
		{
			model = glm::mat4_cast(glm::normalize(glm::qua<GLfloat>(glm::radians(glm::vec3(DynamicModel::roll, DynamicModel::yaw, DynamicModel::pitch))))) * model;
		}
		else
		{
			model = cg::Utility::CalcYaw(yaw) * cg::Utility::CalcPitch(pitch) * cg::Utility::CalcRoll(roll) * model;
		}
		model = glm::translate(model, local_pos);
		global_transform = global_transform * model;
		this->renderer->SetMat4("model", global_transform);
		for (const auto& mesh : mesh_comp->GetMeshes())
		{
			// draw textures for each mesh
			mesh->Draw(this->renderer.get());
		}
	}
}

void cg::DynamicModel::Update(float delta)
{
	float factor = 5.0f;
	float degree = delta * factor;
	auto window = cg::GameInstance::GetInstance()->GetWindowManager()->GetWindow();
	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
	{
		//auto rot = this->rotation_matrix;
		//rot = glm::rotate(rot, this->GetLocalRotation().x, glm::vec3(1.0f, 0.0f, 0.0f));
		//rot = glm::rotate(rot, this->GetLocalRotation().y, glm::vec3(0.0f, 1.0f, 0.0f));
		//rot = glm::rotate(rot, this->GetLocalRotation().z - degree, glm::vec3(0.0f, 0.0f, 1.0f));
		//this->rotation_matrix = rot;
		DynamicModel::pitch -= degree;
	}
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
	{
		//auto rot = this->rotation_matrix;
		//rot = glm::rotate(rot, this->GetLocalRotation().x, glm::vec3(1.0f, 0.0f, 0.0f));
		//rot = glm::rotate(rot, this->GetLocalRotation().y, glm::vec3(0.0f, 1.0f, 0.0f));
		//rot = glm::rotate(rot, this->GetLocalRotation().z + degree, glm::vec3(0.0f, 0.0f, 1.0f));
		//this->rotation_matrix = rot;
		DynamicModel::pitch += degree;
	}
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
	{
		//auto rot = rotation_matrix;
		//rot = glm::rotate(rot, this->GetLocalRotation().x, glm::vec3(1.0f, 0.0f, 0.0f));
		//rot = glm::rotate(rot, this->GetLocalRotation().y + degree, glm::vec3(0.0f, 1.0f, 0.0f));
		//rot = glm::rotate(rot, this->GetLocalRotation().z, glm::vec3(0.0f, 0.0f, 1.0f));
		//this->rotation_matrix = rot;
		DynamicModel::yaw += degree;
	}
	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
	{
		//auto rot = rotation_matrix;
		//rot = glm::rotate(rot, this->GetLocalRotation().x, glm::vec3(1.0f, 0.0f, 0.0f));
		//rot = glm::rotate(rot, this->GetLocalRotation().y - degree, glm::vec3(0.0f, 1.0f, 0.0f));
		//rot = glm::rotate(rot, this->GetLocalRotation().z, glm::vec3(0.0f, 0.0f, 1.0f));
		//this->rotation_matrix = rot;
		DynamicModel::yaw -= degree;
	}
	if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS)
	{
		//auto rot = rotation_matrix;
		//rot = glm::rotate(rot, this->GetLocalRotation().x + degree, glm::vec3(1.0f, 0.0f, 0.0f));
		//rot = glm::rotate(rot, this->GetLocalRotation().y, glm::vec3(0.0f, 1.0f, 0.0f));
		//rot = glm::rotate(rot, this->GetLocalRotation().z, glm::vec3(0.0f, 0.0f, 1.0f));
		//this->rotation_matrix = rot;
		DynamicModel::roll += degree;
	}
	if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS)
	{
		//auto rot = this->rotation_matrix;
		//rot = glm::rotate(rot, this->GetLocalRotation().x - degree, glm::vec3(1.0f, 0.0f, 0.0f));
		//rot = glm::rotate(rot, this->GetLocalRotation().y, glm::vec3(0.0f, 1.0f, 0.0f));
		//rot = glm::rotate(rot, this->GetLocalRotation().z, glm::vec3(0.0f, 0.0f, 1.0f));
		//this->rotation_matrix = rot;
		DynamicModel::roll -= degree;
	}
	if (glfwGetKey(window, GLFW_KEY_Z) == GLFW_PRESS)
	{
		auto rot = this->rotation_matrix;
		rot = glm::rotate(rot, glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
		this->rotation_matrix = rot;
		DynamicModel::pitch = 90.0f;
	}
	if (glfwGetKey(window, GLFW_KEY_X) == GLFW_PRESS)
	{
		DynamicModel::use_quaternion = !DynamicModel::use_quaternion;
	}
	if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS)
	{
		DynamicModel::yaw = 0;
		DynamicModel::pitch = 0;
		DynamicModel::roll = 0;
	}
	//if (glfwGetKey(window, GLFW_KEY_K) == GLFW_PRESS)
	//{
	//	if (!this->cursor_disabled)
	//	{
	//		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	//	}
	//	else
	//	{
	//		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	//	}
	//}
}

float cg::DynamicModel::yaw = 0.0f;
float cg::DynamicModel::roll = 0.0f;
float cg::DynamicModel::pitch = 0.0f;

bool cg::DynamicModel::use_quaternion = false;

std::string cg::DynamicModel::GenerateVertexShaderCode()
{
	std::string result =
		"#version 330 core\n"
		"\n"
		"layout(location = 0) in vec3 aPos; \n"
		"layout(location = 1) in vec3 aNormal; \n"
		"layout(location = 2) in vec2 aTexCoord; \n"
		"\n"
		"out vec2 TexCoord; \n"
		"out vec3 Normal; \n"
		"out vec3 FragPos; \n"
		"\n"
		"uniform mat4 model; \n"
		"uniform mat4 view; \n"
		"uniform mat4 projection; \n"
		"\n"
		"void main()\n"
		"{ \n"
		"    vec4 LocalPos = vec4(aPos, 1.0); \n"
		"    gl_Position = projection * view * model * LocalPos; \n"
		"    TexCoord = aTexCoord; \n"
		"    Normal = mat3(transpose(inverse(view * model))) * aNormal; \n"
		"    FragPos = vec3(view * model * vec4(aPos, 1.0)); \n"
		"}\n";
	return result;
}

std::string cg::DynamicModel::GenerateFragmentShaderCode()
{
	return "#version 330 core\n"
		"out vec4 FragColor;\n"
		"\n"
		"in vec2 TexCoord;\n"
		"in vec3 Normal;\n"
		"in vec3 FragPos;\n"
		"\n"
		"struct Material\n"
		"{\n"
		"sampler2D diffuse;\n"
		"sampler2D specular;\n"
		"float metallic;\n"
		"float subsurface;\n"
		"float roughness;\n"
		"float specularTint;\n"
		"float anisotropic;\n"
		"float sheen;\n"
		"float sheenTint;\n"
		"float clearcoat;\n"
		"float clearcoatGloss;\n"
		"};\n"
		"\n"
		"struct PointLight\n"
		"{\n"
		"vec3 position;\n"
		"vec3 direction;\n"
		"vec3 ambient;\n"
		"vec3 diffuse;\n"
		"vec3 specular;\n"
		"float intensity;\n"
		"float constant;\n"
		"float linear;\n"
		"float quadratic;\n"
		"};\n"
		"\n"
		"uniform Material material;\n"
		"uniform PointLight light;\n"
		"\n"
		"vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir)\n"
		"{\n"
		"vec3 lightDir = normalize(light.position - fragPos);\n"
		"float diff = max(dot(normal, lightDir), 0.0);\n"
		"vec3 texDiff = vec3(texture(material.diffuse, TexCoord));\n"
		"vec3 diffuse = light.diffuse * diff * texDiff;\n"
		"vec3 ambient = light.ambient * texDiff;\n"
		"vec3 reflectDir = reflect(-lightDir, normal);\n"
		"float shininess = 32.0;\n"
		"float texSpec = float(texture(material.specular, TexCoord));\n"
		"vec3 specular = light.specular * (pow(max(dot(viewDir, reflectDir), 0.0), shininess) * texSpec);"
		"float dis = length(light.position - FragPos);\n"
		"float attenuation = 1.0 / (light.constant + light.linear * dis + light.quadratic * (dis * dis));\n"
		"return attenuation * (ambient + diffuse + specular);\n"
		"}\n"
		"\n"
		"void main()\n"
		"{\n"
		"vec3 norm = normalize(Normal);\n"
		"vec3 viewDir = normalize(-FragPos);\n"
		"FragColor = vec4(CalcPointLight(light, norm, FragPos, viewDir), 1.0);\n"
		"}\n";
}
