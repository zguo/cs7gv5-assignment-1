#include "AStaticModel.h"
#include "GameInstance.h"

cg::StaticMeshActor::StaticMeshActor(const std::string& path)
{
	this->renderer->ActivateContext();
	this->mesh_comp = std::make_shared<cg::StaticMeshComponent>(path);
	if (this->mesh_comp->GetBoundingBox())
	{
		this->bounding_box = this->mesh_comp->GetBoundingBox();
	}
}

void cg::StaticMeshActor::Render(float delta_time)
{
	// update the model matrix
	auto model = glm::mat4(1.0f);
	auto rotate = this->GetLocalRotation();
	model = glm::scale(model, this->GetLocalScale());
	// model = glm::rotate(model, rotate.x, glm::vec3(1.0f, 0.0f, 0.0f));
	// model = glm::rotate(model, rotate.y, glm::vec3(0.0f, 1.0f, 0.0f));
	// model = glm::rotate(model, rotate.z, glm::vec3(0.0f, 0.0f, 1.0f));
	model = glm::translate(model, this->GetGlobalPosition());
	model = glm::mat4_cast(glm::normalize(glm::qua<GLfloat>(glm::radians(rotate)))) * model;
	this->renderer->SetMat4("model", model);
	for (const auto& mesh : this->mesh_comp->GetMeshes())
	{
		mesh->Draw(this->renderer.get());
	}
}
