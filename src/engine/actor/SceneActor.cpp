#include "SceneActor.h"
#include "GameInstance.h"

cg::SceneActor::SceneActor(bool b_use_default_renderer)
{
	// use default renderer by default
	// otherwise, let derived class construct a customised one
	if (b_use_default_renderer)
	{
		auto manager = cg::GameInstance::GetInstance()->GetRendererManager();
		this->renderer = manager->GetDefaultRenderer();
	}
}
