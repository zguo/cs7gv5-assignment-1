#include "Actor.h"
#include "CTransform.h"

bool cg::Actor::RemoveComponentByName(const std::string& component_name)
{
	auto iter = std::find_if(this->components.begin(), this->components.end(), [&component_name](const std::shared_ptr<cg::Component> component)
		{
			return component->GetName().compare(component_name) == 0;
		}
	);
	if (iter == this->components.end())
	{
		return false;
	}
	else
	{
		try
		{
			this->components.erase(iter);
			return true;
		}
		catch (...)
		{
			return false;
		}
	}
}

bool cg::Actor::RemoveComponentByUUID(const std::string& uuid)
{
	auto iter = std::find_if(this->components.begin(), this->components.end(), [&uuid](const std::shared_ptr<cg::Component> component)
		{
			return component->GetUUID().compare(uuid) == 0;
		}
	);
	if (iter == this->components.end())
	{
		return false;
	}
	else
	{
		try
		{
			this->components.erase(iter);
			return true;
		}
		catch (...)
		{
			return false;
		}
	}
}

std::shared_ptr<cg::Component> cg::Actor::GetComponentByUUID(const std::string& uuid) const
{
	auto iter = std::find_if(this->components.begin(), this->components.end(), [&uuid](const std::shared_ptr<cg::Component> component)
		{
			return component->GetUUID().compare(uuid) == 0;
		}
	);
	if (iter == this->components.end())
	{
		return nullptr;
	}
	else
	{
		return *iter;
	}
}

std::shared_ptr<cg::Component> cg::Actor::GetComponentByName(const std::string& name) const
{
	auto iter = std::find_if(this->components.begin(), this->components.end(), [&name](const std::shared_ptr<cg::Component> component)
		{
			return component->GetName().compare(name) == 0;
		}
	);
	if (iter == this->components.end())
	{
		return nullptr;
	}
	else
	{
		return *iter;
	}
}
