#include "GameObject.h"
#include "ILifeCycleEvents.h"
#include "IDisableUpdate.h"

std::string cg::GameObject::GetName() const
{
	return this->name;
}

void cg::GameObject::SetName(const std::string& name)
{
	this->name = name;
}

void cg::GameObject::Update(float delta)
{
}