#include "CCamera.h"

cg::CCamera::CCamera(glm::vec3 local_position, glm::vec3 local_rotation)
{
	this->transform_comp = std::make_shared<cg::CTransform>();
	this->transform_comp->SetLocalPosition(local_position);
	this->transform_comp->SetLocalRotation(local_rotation);
}

glm::vec3 cg::CCamera::GetLocalPosition() const
{
	return this->transform_comp->GetLocalPosition();
}

void cg::CCamera::SetLocalPosition(glm::vec3 position)
{
	this->transform_comp->SetLocalPosition(position);
}

glm::vec3 cg::CCamera::GetLocalRotation() const
{
	return this->transform_comp->GetLocalRotation();
}

void cg::CCamera::SetLocalRotation(glm::vec3 rotation)
{
	this->transform_comp->SetLocalRotation(rotation);
}

