#include "CDynamicMesh.h"
#include "Utility.h"
#include "GameInstance.h"

cg::DynamicMeshComponent::DynamicMeshComponent(const std::string &path)
{
	this->LoadModel(path);
	if (this->textures.size() == 0)
	{
		// assign a default diffuse map and no specular map to each mesh
		auto texture = cg::Utility::LoadDefaultTexture();
		this->textures.push_back(texture);
		for (const auto &mesh : this->meshes)
		{
			mesh->AddTexture(texture);
		}
	}
	// generate bounding box
	this->bounding_box = std::make_shared<cg::BoundingBox>();
	this->bounding_box->coordinates.push_back(glm::vec3(this->min_x, this->min_y, this->max_z));
	this->bounding_box->coordinates.push_back(glm::vec3(this->min_x, this->min_y, this->min_z));
	this->bounding_box->coordinates.push_back(glm::vec3(this->min_x, this->max_y, this->max_z));
	this->bounding_box->coordinates.push_back(glm::vec3(this->min_x, this->max_y, this->min_z));
	this->bounding_box->coordinates.push_back(glm::vec3(this->max_x, this->min_y, this->max_z));
	this->bounding_box->coordinates.push_back(glm::vec3(this->max_x, this->min_y, this->min_z));
	this->bounding_box->coordinates.push_back(glm::vec3(this->max_x, this->max_y, this->max_z));
	this->bounding_box->coordinates.push_back(glm::vec3(this->max_x, this->max_y, this->min_z));
	this->bounding_box->min_x = this->min_x;
	this->bounding_box->max_x = this->max_x;
	this->bounding_box->min_y = this->min_y;
	this->bounding_box->max_y = this->max_y;
	this->bounding_box->min_z = this->min_z;
	this->bounding_box->max_z = this->max_z;
	cg::IPhysics::AddBoundingBox(this->bounding_box);
}

void cg::DynamicMeshComponent::PlayAnimation(float delta_time)
{
	if (this->b_enable_animation)
		this->animation->Play(delta_time);
}

void cg::DynamicMeshComponent::LoadModel(const std::string &path)
{
	this->scene = this->importer.ReadFile(path,
										  aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_GenNormals
										  //| aiProcess_MakeLeftHanded // if use left-handed coordinate system
	);
	if (!this->scene || this->scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !this->scene->mRootNode)
	{
		std::cout << "Error loading model: " << this->importer.GetErrorString() << std::endl;
		return;
	}
	unsigned int num_vertices = 0;
	unsigned int num_indices = 0;
	for (auto i = 0; i < this->scene->mNumMeshes; i++)
	{
		auto indices_count = 0;
		for (auto j = 0; j < this->scene->mMeshes[i]->mNumFaces; j++)
		{
			indices_count += this->scene->mMeshes[i]->mFaces[j].mNumIndices;
		}
		num_vertices += this->scene->mMeshes[i]->mNumVertices;
		num_indices += indices_count;
	}
	this->ProcessNode(scene->mRootNode, scene);
}

void cg::DynamicMeshComponent::ProcessNode(aiNode *node, const aiScene *scene)
{
	for (auto i = 0; i < node->mNumMeshes; i++)
	{
		auto ptr = this->ProcessMesh(scene->mMeshes[node->mMeshes[i]], scene);
		if (ptr)
		{
			this->meshes.push_back(this->ProcessMesh(scene->mMeshes[node->mMeshes[i]], scene));
		}
	}
	for (auto i = 0; i < node->mNumChildren; i++)
	{
		this->ProcessNode(node->mChildren[i], scene);
	}
}

std::shared_ptr<cg::DynamicMesh> cg::DynamicMeshComponent::ProcessMesh(aiMesh *mesh, const aiScene *scene)
{
	std::vector<cg::DynamicMeshVertex> vertices;
	std::vector<GLuint> indices;
	std::vector<cg::Texture> textures;
	cg::DynamicMeshVertex vertex;

	// float left = 0.0f;
	// float right = 0.0f;
	// float front = 0.0f;
	// float back = 0.0f;
	// float bottom = 0.0f;
	// float top = 0.0f;
	// for (auto i = 0;i <mesh->mNumVertices;i++)
	// {
	// 	left = mesh->mVertices[i].x < left ? mesh->mVertices[i].x : left;
	// 	right = mesh->mVertices[i].x > right ? mesh->mVertices[i].x : right;
	// 	front = mesh->mVertices[i].z > front ? mesh->mVertices[i].z : front;
	// 	back = mesh->mVertices[i].z < back ? mesh->mVertices[i].z : back;
	// 	bottom = mesh->mVertices[i].y < bottom ? mesh->mVertices[i].y : bottom;
	// 	top = mesh->mVertices[i].y > top ? mesh->mVertices[i].y : top;
	// }
	// auto iter = this->bounding_box->coordinates.begin();
	// this->bounding_box->coordinates.push_back(glm::vec3(left, bottom, front));
	// this->bounding_box->coordinates.push_back(glm::vec3(left, bottom, back));
	// this->bounding_box->coordinates.push_back(glm::vec3(left, top, front));
	// this->bounding_box->coordinates.push_back(glm::vec3(left, top, back));
	// this->bounding_box->coordinates.push_back(glm::vec3(right, bottom, front));
	// this->bounding_box->coordinates.push_back(glm::vec3(right, bottom, back));
	// this->bounding_box->coordinates.push_back(glm::vec3(right, top, front));
	// this->bounding_box->coordinates.push_back(glm::vec3(right, top, back));

	// this->bounding_box->min_x = left;
	// this->bounding_box->max_x = right;
	// this->bounding_box->min_y = bottom;
	// this->bounding_box->max_y = top;
	// this->bounding_box->min_z = back;
	// this->bounding_box->max_z = front;

	// return nullptr;

	for (int i = 0; i < mesh->mNumVertices; i++)
	{
		this->max_x = mesh->mVertices[i].x > this->max_x ? mesh->mVertices[i].x : this->max_x;
		this->min_x = mesh->mVertices[i].x < this->min_x ? mesh->mVertices[i].x : this->min_x;
		this->max_y = mesh->mVertices[i].y > this->max_y ? mesh->mVertices[i].y : this->max_y;
		this->min_y = mesh->mVertices[i].y < this->min_y ? mesh->mVertices[i].y : this->min_y;
		this->max_z = mesh->mVertices[i].z > this->max_z ? mesh->mVertices[i].z : this->max_z;
		this->min_z = mesh->mVertices[i].z < this->min_z ? mesh->mVertices[i].z : this->min_z;
	}
	// vertices
	for (auto i = 0; i < mesh->mNumVertices; i++)
	{
		glm::vec3 position(0.0f), normal(0.0f);
		glm::vec2 tex_coord(0.0f);
		position.x = mesh->mVertices[i].x;
		position.y = mesh->mVertices[i].y;
		position.z = mesh->mVertices[i].z;
		//TODO：是否需要添加 mesh->HasNormals 做判断？
		normal.x = mesh->mNormals[i].x;
		normal.y = mesh->mNormals[i].y;
		normal.z = mesh->mNormals[i].z;
		if (mesh->mTextureCoords[0])
		{
			tex_coord.x = mesh->mTextureCoords[0][i].x;
			tex_coord.y = mesh->mTextureCoords[0][i].y;
		}
		vertex.position = position;
		vertex.normal = normal;
		vertex.texture_coord = tex_coord;
		vertices.push_back(vertex);
	}
	// indices
	for (auto i = 0; i < mesh->mNumFaces; i++)
	{
		aiFace face = mesh->mFaces[i];
		// TODO: if mNumIndices != 3, when drawing triangles using element buffer, there may be errors
		// so make sure that all faces are triangles when loading models
		for (auto j = 0; j < face.mNumIndices; j++)
		{
			indices.push_back(face.mIndices[j]);
		}
	}
	// textures
	if (mesh->mMaterialIndex >= 0)
	{
		// TODO: add other textures
		aiMaterial *material = scene->mMaterials[mesh->mMaterialIndex];
		std::vector<cg::Texture> diffuse = this->LoadTextures(scene, material, aiTextureType_DIFFUSE, cg::ETextureType::DIFFUSE_MAP);
		textures.insert(textures.end(), diffuse.begin(), diffuse.end());
		std::vector<cg::Texture> specular = this->LoadTextures(scene, material, aiTextureType_SPECULAR, cg::ETextureType::SPECULAR_MAP);
		textures.insert(textures.end(), specular.begin(), specular.end());
	}
	auto result = std::make_shared<cg::DynamicMesh>(vertices, indices, textures);
	return result;
}

std::vector<cg::Texture> cg::DynamicMeshComponent::LoadTextures(const aiScene *scene, aiMaterial *mat, aiTextureType type, cg::ETextureType cg_type)
{
	// TODO: change return value to a pointer
	// TODO: it seems that this snippet utilises external texture files (because of the 'path' parameter when loading)
	// consider what to do with internal textures
	std::vector<cg::Texture> textures;
	for (auto i = 0; i < mat->GetTextureCount(type); i++)
	{
		aiString s;
		mat->GetTexture(type, i, &s);
		std::string path = /*cg::RESOURCE_DIR + "test\\" + */ s.C_Str();
		bool flag = false;
		for (auto j = 0; j < this->textures.size(); j++)
		{
			if (std::strcmp(this->textures[j].path.data(), s.C_Str()) == 0)
			{
				textures.push_back(this->textures[j]);
				flag = true;
				break;
			}
		}
		// only load texture if it has not been loaded
		if (!flag)
		{
			auto p = scene->GetEmbeddedTexture(s.C_Str());
			cg::Texture texture(
				path,
				p ? cg::Utility::LoadTextureEmbedded(p, GL_REPEAT, GL_LINEAR, GL_LINEAR, cg_type) : cg::Utility::LoadTextureFromFile(path, GL_REPEAT, GL_LINEAR, GL_LINEAR, cg_type),
				cg_type);
			textures.push_back(texture);
			this->textures.push_back(texture);
		}
	}
	return textures;
}