#include "CStaticMesh.h"
#include "Utility.h"
#include "GameInstance.h"

cg::StaticMeshComponent::StaticMeshComponent(const std::string &path)
{
	this->LoadModel(path);
	if (this->textures.size() == 0)
	{
		// assign a default diffuse map and no specular map to each mesh
		auto texture = cg::Utility::LoadDefaultTexture();
		this->textures.push_back(texture);
		for (const auto &mesh : this->meshes)
		{
			mesh->AddTexture(texture);
		}
	}
}

void cg::StaticMeshComponent::LoadModel(const std::string &path)
{
	Assimp::Importer importer;
	auto scene = importer.ReadFile(path,
								   aiProcess_Triangulate | aiProcess_FlipUVs
								   //| aiProcess_MakeLeftHanded // if use left-handed coordinate system
	);
	if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
	{
		std::cout << "Error loading model: " << importer.GetErrorString() << std::endl;
		return;
	}
	this->ProcessNode(scene->mRootNode, scene);
}

void cg::StaticMeshComponent::ProcessNode(aiNode *node, const aiScene *scene)
{
	for (auto i = 0; i < node->mNumMeshes; i++)
	{
		auto ptr = this->ProcessMesh(scene->mMeshes[node->mMeshes[i]], scene);
		if (ptr)
		{
			this->meshes.push_back(this->ProcessMesh(scene->mMeshes[node->mMeshes[i]], scene));
		}
	}
	for (auto i = 0; i < node->mNumChildren; i++)
	{
		this->ProcessNode(node->mChildren[i], scene);
	}
}

std::shared_ptr<cg::StaticMesh> cg::StaticMeshComponent::ProcessMesh(aiMesh *mesh, const aiScene *scene)
{
	std::vector<cg::Vertex> vertices;
	std::vector<GLuint> indices;
	std::vector<cg::Texture> textures;

	// vertices
	for (auto i = 0; i < mesh->mNumVertices; i++)
	{
		glm::vec3 vertex(0.0f), normal(0.0f);
		glm::vec2 tex_coord(0.0f);
		vertex.x = mesh->mVertices[i].x;
		vertex.y = mesh->mVertices[i].y;
		vertex.z = mesh->mVertices[i].z;
		normal.x = mesh->mNormals[i].x;
		normal.y = mesh->mNormals[i].y;
		normal.z = mesh->mNormals[i].z;
		if (mesh->mTextureCoords[0])
		{
			tex_coord.x = mesh->mTextureCoords[0][i].x;
			tex_coord.y = mesh->mTextureCoords[0][i].y;
		}
		vertices.push_back(cg::Vertex(vertex, tex_coord, normal));
	}
	// indices
	for (auto i = 0; i < mesh->mNumFaces; i++)
	{
		aiFace face = mesh->mFaces[i];
		for (auto j = 0; j < face.mNumIndices; j++)
		{
			indices.push_back(face.mIndices[j]);
		}
	}
	// textures
	if (mesh->mMaterialIndex >= 0)
	{
		// TODO: add other textures
		aiMaterial *material = scene->mMaterials[mesh->mMaterialIndex];
		std::vector<cg::Texture> diffuse = this->LoadTextures(scene, material, aiTextureType_DIFFUSE, cg::ETextureType::DIFFUSE_MAP);
		textures.insert(textures.end(), diffuse.begin(), diffuse.end());
		std::vector<cg::Texture> specular = this->LoadTextures(scene, material, aiTextureType_SPECULAR, cg::ETextureType::SPECULAR_MAP);
		textures.insert(textures.end(), specular.begin(), specular.end());
	}
	return std::make_shared<cg::StaticMesh>(vertices, indices, textures);
}

std::vector<cg::Texture> cg::StaticMeshComponent::LoadTextures(const aiScene *scene, aiMaterial *mat, aiTextureType type, cg::ETextureType cg_type)
{
	// TODO: change return value to a pointer
	// TODO: it seems that this snippet utilises external texture files (because of the 'path' parameter when loading)
	// consider what to do with internal textures
	std::vector<cg::Texture> textures;
	for (auto i = 0; i < mat->GetTextureCount(type); i++)
	{
		aiString s;
		mat->GetTexture(type, i, &s);
		std::string path = /*cg::RESOURCE_DIR + "test\\" + */ s.C_Str();
		bool flag = false;
		for (auto j = 0; j < this->textures.size(); j++)
		{
			if (std::strcmp(this->textures[j].path.data(), s.C_Str()) == 0)
			{
				textures.push_back(this->textures[j]);
				flag = true;
				break;
			}
		}
		// only load texture if it has not been loaded
		if (!flag)
		{
			auto p = scene->GetEmbeddedTexture(s.C_Str());
			cg::Texture texture(
				path,
				p ? cg::Utility::LoadTextureEmbedded(p, GL_REPEAT, GL_LINEAR, GL_LINEAR, cg_type) : cg::Utility::LoadTextureFromFile(path, GL_REPEAT, GL_LINEAR, GL_LINEAR, cg_type),
				cg_type);
			textures.push_back(texture);
			this->textures.push_back(texture);
		}
	}
	return textures;
}