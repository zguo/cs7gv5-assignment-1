#include "CTransform.h"
#include "Utility.h"
#include "Logger.h"

cg::CTransform::CTransform() :
	local_position(glm::vec3(0.0f)),
	global_position(glm::vec3(0.0f)),
	local_rotation(glm::vec3(0.0f)),
	local_scale(glm::vec3(1.0f))
{

}

glm::vec3 cg::CTransform::GetLocalPosition() const
{
	return local_position;
}

void cg::CTransform::SetLocalPosition(glm::vec3 val)
{
	local_position = val;
}

void cg::CTransform::SetLocalPosition(float x, float y, float z)
{
	local_position = glm::vec3(x, y, z);
}

glm::vec3 cg::CTransform::GetGlobalPosition() const
{
	return global_position;
}

void cg::CTransform::SetGlobalPosition(glm::vec3 val)
{
	global_position = val;
}

void cg::CTransform::SetGlobalPosition(float x, float y, float z)
{
	global_position = glm::vec3(x, y, z);
}

glm::vec3 cg::CTransform::GetLocalRotation() const
{
	return this->local_rotation;
}

void cg::CTransform::SetLocalRotation(glm::vec3 val)
{
	this->local_rotation = val;
}

void cg::CTransform::SetLocalRotation(float x, float y, float z)
{
	local_position = glm::vec3(x, y, z);
}

void cg::CTransform::RotateAround(float degree, glm::vec3 axis)
{
	auto current_rot = glm::qua<float>(this->local_rotation);
	auto new_rot = glm::rotate(current_rot, glm::radians(degree), axis);
	this->local_rotation = glm::eulerAngles(new_rot);
	cg::Logger::Log(this->local_rotation);
}

glm::vec3 cg::CTransform::GetGlobalRotation() const
{
	return this->global_rotation;
}

void cg::CTransform::SetGlobalRotation(glm::vec3 val)
{
	this->global_rotation = val;
}

void cg::CTransform::SetGlobalRotation(float x, float y, float z)
{
	this->global_rotation = glm::vec3(x, y, z);
}

glm::vec3 cg::CTransform::GetLocalScale() const
{
	return local_scale;
}

void cg::CTransform::SetLocalScale(glm::vec3 val)
{
	local_scale = val;
}

glm::vec3 cg::CTransform::GetGlobalScale() const
{
	return this->global_scale;
}

void cg::CTransform::SetGlobalScale(float x, float y, float z)
{
	this->global_scale = glm::vec3(x, y, z);
}

void cg::CTransform::SetGlobalScale(glm::vec3 val)
{
	this->global_scale = val;
}

void cg::CTransform::SetLocalScale(float x, float y, float z)
{
	local_position = glm::vec3(x, y, z);
}
