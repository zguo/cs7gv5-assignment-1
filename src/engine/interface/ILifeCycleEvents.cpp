#include "ILifeCycleEvents.h"
#include "Logger.h"

std::vector<cg::ILifeCycleEvents*> cg::ILifeCycleEvents::update_list;

void cg::ILifeCycleEvents::Destroy(cg::ILifeCycleEvents* p)
{
	// note that a reference to a intelligent pointer is used here
	// this is because we want to manipulate the EXACT pointer object in the container
	auto iter = std::find_if(cg::ILifeCycleEvents::update_list.begin(), cg::ILifeCycleEvents::update_list.end(), [p](const ILifeCycleEvents* ptr)
		{
			return p == ptr;
		}
	);
	if (iter == cg::ILifeCycleEvents::update_list.end())
	{
		 Logger::Log("[Error] trying to erase a non-existing pointer");
	}
	else
	{
		cg::ILifeCycleEvents::update_list.erase(iter);
		(*iter) = nullptr;
	}
}