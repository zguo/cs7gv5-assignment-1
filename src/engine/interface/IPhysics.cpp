#include "IPhysics.h"

void cg::IPhysics::AddBoundingBox(std::shared_ptr<cg::BoundingBox> bb)
{
	cg::IPhysics::ALL_BOUNDING_BOXES.push_back(bb);
}

std::vector<std::shared_ptr<cg::BoundingBox>> cg::IPhysics::ALL_BOUNDING_BOXES = std::vector<std::shared_ptr<cg::BoundingBox>>();
