#include "OpenGLAdapter.h"

void cg::OpenGLAdapter::SetVec3(const std::string& name, glm::vec3 value)
{
	GLint location = glGetUniformLocation(this->program_id, name.c_str());
	if (location == -1)
		cg::Logger::Log("Error trying to find uniform location of " + name);
	glUseProgram(this->program_id);
	glUniform3f(location, value.x, value.y, value.z);
}

void cg::OpenGLAdapter::SetMat4(const std::string& name, glm::mat4 value)
{
	GLint location = glGetUniformLocation(this->program_id, name.c_str());
	if (location == -1)
		cg::Logger::Log("Error trying to find uniform location of " + name);
	glUseProgram(this->program_id);
	glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(value));
}

void cg::OpenGLAdapter::SetInt(const std::string& name, GLint value)
{
	GLint location = glGetUniformLocation(this->program_id, name.c_str());
	if (location == -1)
		cg::Logger::Log("Error trying to find uniform location of " + name);
	glUseProgram(this->program_id);
	glUniform1i(location, value);
}

void cg::OpenGLAdapter::SetFloat(const std::string& name, GLfloat value)
{
	GLint location = glGetUniformLocation(this->program_id, name.c_str());
	if (location == -1)
		cg::Logger::Log("Error trying to find uniform location of " + name);
	glUseProgram(this->program_id);
	glUniform1f(location, value);
}

cg::OpenGLAdapter::OpenGLAdapter(const std::string& vertex_shader_code, const std::string& fragment_shader_code)
{
	// create program
	//
	this->program_id = glCreateProgram();

	// add shaders
	//
	GLuint vs_id, fs_id;
	int success;
	char log[512];
	const GLchar* vs = vertex_shader_code.c_str();
	const GLchar* fs = fragment_shader_code.c_str();
	// vertex shader
	vs_id = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vs_id, 1, &vs, nullptr);
	glCompileShader(vs_id);
	glGetShaderiv(vs_id, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(vs_id, 512, nullptr, log);
		Logger::Log("Error when trying to compile shader: " + std::string(log));
	};
	// fragment shader
	fs_id = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fs_id, 1, &fs, nullptr);
	glCompileShader(fs_id);
	glGetShaderiv(fs_id, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(fs_id, 512, nullptr, log);
		Logger::Log("Error when trying to compile shader: " + std::string(log));
	};
	// linking
	//
	glAttachShader(this->program_id, vs_id);
	glAttachShader(this->program_id, fs_id);
	glLinkProgram(this->program_id);
	glGetProgramiv(this->program_id, GL_LINK_STATUS, &success);
	if (success == 0)
	{
		glGetProgramInfoLog(this->program_id, sizeof(log), nullptr, log);
		Logger::Log("Error while linking shader program. " + std::string(log));
	}
	glDeleteShader(vs_id);
	glDeleteShader(fs_id);
}

cg::OpenGLAdapter::OpenGLAdapter() :program_id(0)
{

}

void cg::OpenGLAdapter::Use() const
{
	glUseProgram(this->program_id);
}

cg::OpenGLAdapter::~OpenGLAdapter() = default;
