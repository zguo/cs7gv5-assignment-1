#include "stb/stb_image.h"

#include "Renderer.h"
#include "OpenGLAdapter.h"

cg::Renderer::Renderer(const std::string& vertex_shader_code, const std::string& fragment_shader_code)
{
	// TODO: choose a certain backend
	this->adapter = std::make_shared<cg::OpenGLAdapter>(vertex_shader_code, fragment_shader_code);
}

cg::Renderer::Renderer() = default;

void cg::Renderer::ActivateContext() const
{
	this->adapter->Use();
}

void cg::Renderer::SetFloat(const std::string& name, GLfloat value) const
{
	this->ActivateContext();
	this->adapter->SetFloat(name, value);
}

void cg::Renderer::SetInt(const std::string& name, GLint value) const
{
	this->ActivateContext();
	this->adapter->SetInt(name, value);
}

void cg::Renderer::SetMat4(const std::string& name, glm::mat4 value) const
{
	this->ActivateContext();
	this->adapter->SetMat4(name, value);
}

void cg::Renderer::SetVec3(const std::string& name, glm::vec3 value) const
{
	this->ActivateContext();
	this->adapter->SetVec3(name, value);
}
