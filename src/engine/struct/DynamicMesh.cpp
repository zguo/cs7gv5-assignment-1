#include "SDynamicMesh.h"

cg::DynamicMesh::DynamicMesh(const std::vector<cg::DynamicMeshVertex>& vertices, const std::vector<GLuint>& indices, const std::vector<cg::Texture>& textures) :
	vertices(vertices),
	indices(indices),
	textures(textures)
{
	glGenVertexArrays(1, &this->vao);
	glGenBuffers(1, &this->vbo);
	glGenBuffers(1, &this->ebo);

	glBindVertexArray(this->vao);
	glBindBuffer(GL_ARRAY_BUFFER, this->vbo);

	glBufferData(GL_ARRAY_BUFFER, this->vertices.size() * sizeof(this->vertices[0]), &this->vertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->indices.size() * sizeof(GLuint), &this->indices[0], GL_STATIC_DRAW);

	// position
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(this->vertices[0]), (void*)offsetof(cg::DynamicMeshVertex, position));
	// normal
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(this->vertices[0]), (void*)offsetof(cg::DynamicMeshVertex, normal));
	// texture coords
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(this->vertices[0]), (void*)offsetof(cg::DynamicMeshVertex, texture_coord));

	glBindVertexArray(0);
}

void cg::DynamicMesh::Draw(const cg::Renderer* renderer)
{
	std::string name = "";
	for (auto i = 0; i < this->textures.size(); i++)
	{
		name = "";
		glActiveTexture(GL_TEXTURE0 + i);
		switch (this->textures[i].type)
		{
		case cg::ETextureType::DIFFUSE_MAP:
			name = "diffuse";
			break;
		case cg::ETextureType::SPECULAR_MAP:
			name = "specular";
			break;
		default:
			break;
		}
		if (name == "")
		{
			std::cout << "Error setting textures" << std::endl;
		}
		else
		{
			renderer->SetInt(("material." + name).c_str(), i);
			glBindTexture(GL_TEXTURE_2D, this->textures[i].id);
		}
	}
	glBindVertexArray(this->vao);
	glDrawElements(GL_TRIANGLES, this->indices.size(), GL_UNSIGNED_INT, nullptr);
	glBindVertexArray(0);
}