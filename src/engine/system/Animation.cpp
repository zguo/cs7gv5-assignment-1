#include "Animation.h"
#include "CDynamicMesh.h"

cg::Animation::Animation(std::shared_ptr<cg::DynamicMeshComponent> mesh_comp, glm::vec3 axis, float speed, bool direction, bool persistent, int duration_in_second /*= 0*/) :
	mesh_comp(mesh_comp),
	time_elapsed(0.0f),
	total_time(0.0f),
	speed(speed),
	axis(axis),
	direction(direction),
	persistent(persistent),
	duration(duration_in_second),
	preset(cg::AnimationPreset::SIMPLE_ROTATION)
{}

void cg::Animation::Play(float delta_time)
{
	switch (this->preset)
	{
	case cg::AnimationPreset::SIMPLE_ROTATION:
		this->PlaySimpleRotation(delta_time);
		break;

	default:
		break;
	}
}

void cg::Animation::PlaySimpleRotation(float delta_time)
{
	if (this->persistent || this->total_time < this->duration)
	{
		auto ptr = this->mesh_comp.lock();
		if (ptr)
		{
			auto pos = ptr->GetLocalPosition();
			auto rot = ptr->GetLocalRotation();
			auto scale = ptr->GetLocalScale();
			auto rot_quat = glm::normalize(glm::qua<GLfloat>(glm::radians(rot)));
			auto new_rot = glm::rotate(rot_quat, glm::radians((delta_time / this->speed) * 360), this->axis);
			ptr->SetLocalRotation(glm::degrees(glm::eulerAngles(new_rot)));
			this->total_time += delta_time;
		}
	}
	else
	{
		auto ptr = this->mesh_comp.lock();
		if (ptr)
		{
			ptr->DisableAnimation();
			this->total_time = 0.0f;
		}
	}
}
