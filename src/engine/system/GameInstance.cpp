#include "GameInstance.h"
#include "Scene.h"
#include "Renderer.h"
#include "Utility.h"
#include "IDisableUpdate.h"

std::shared_ptr<cg::GameInstance> cg::GameInstance::instance;

void cg::GameInstance::Destroy(cg::GameInstance* instance)
{
	delete instance;
}

std::map<std::string, std::unique_ptr<cg::GameObject>> cg::GameInstance::objects;


std::shared_ptr<cg::GameInstance> cg::GameInstance::GetInstance()
{
	if (cg::GameInstance::instance.get() == nullptr)
	{
		cg::GameInstance::instance = std::shared_ptr<cg::GameInstance>(new cg::GameInstance, cg::GameInstance::Destroy);
		cg::GameInstance::instance->Init();
	}
	return cg::GameInstance::instance;
}

std::shared_ptr<cg::PlayerController> cg::GameInstance::GetPlayerController() const
{
	return this->player_controller;
}

std::shared_ptr<cg::WindowManager> cg::GameInstance::GetWindowManager() const
{
	return this->window_manager;
}

std::shared_ptr<cg::RendererManager> cg::GameInstance::GetRendererManager() const
{
	return this->renderer_manager;
}

std::shared_ptr<cg::SceneManager> cg::GameInstance::GetSceneManager() const
{
	return this->scene_manager;
}

void cg::GameInstance::InvokeUpdate(float delta)
{
	for (const auto& p : cg::ILifeCycleEvents::update_list)
	{
		if (!p->IsDerivedFrom<cg::IDisableUpdate>())
		{
			p->Update(delta);
		}
	}
}

void cg::GameInstance::RemoveObject(const GameObject* object)
{
	GameInstance::objects.erase(object->GetUUID());
}

void cg::GameInstance::Init()
{
	auto window = cg::Utility::Init();
	this->window_manager = std::make_shared<cg::WindowManager>(window, (float)SCR_WIDTH, (float)SCR_HEIGHT);
	this->scene_manager = std::make_shared<cg::SceneManager>();
	this->player_controller = std::make_shared<cg::PlayerController>();
	this->renderer_manager = std::make_shared<cg::RendererManager>();
}

void cg::GameInstance::Run()
{
	while (!glfwWindowShouldClose(this->window_manager->GetWindow()))
	{
		float current_frame = glfwGetTime();
		delta_time = current_frame - last_frame;
		last_frame = current_frame;

		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();

		static float yaw = 0.0f;
		static float roll = 0.0f;
		static float pitch = 0.0f;
		ImGui::Begin("Euler Angles");
		ImGui::SliderFloat("yaw", &cg::DynamicModel::yaw, -90.0f, 90.0f);
		ImGui::SliderFloat("roll", &cg::DynamicModel::roll, -90.0f, 90.0f);
		ImGui::SliderFloat("pitch", &cg::DynamicModel::pitch, -90.0f, 90.0f);
		ImGui::RadioButton("use quaternion", cg::DynamicModel::use_quaternion);
		ImGui::End();

		glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		this->InvokeUpdate(delta_time);
		this->scene_manager->GetCurrentScene()->RenderAll(delta_time);

		ImGui::Render();
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

		glfwSwapBuffers(this->window_manager->GetWindow());
		glfwPollEvents();
	}
	
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();

	glfwTerminate();
}

void cg::GameInstance::Terminate()
{
	glfwTerminate();
}
