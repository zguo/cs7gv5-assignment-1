#include "Light.h"
#include "GameInstance.h"

void cg::Light::Render(float delta_time)
{
	// for lights, rendering basically means set some uniforms
	// note light rendering should be finished before drawing any actual models
	// set position
	auto renderer_manager = cg::GameInstance::GetInstance()->GetRendererManager();
	glm::vec3 position;
	if (this->b_calc_in_view_space)
	{
		auto pos = this->transform_comp->GetGlobalPosition();
		glm::vec4 pos4 = this->view * glm::vec4(pos.x, pos.y, pos.z, 1.0f);
		glm::vec3 pos3 = glm::vec3(pos4.x, pos4.y, pos4.z);
		position = pos3;
		this->renderer->SetVec3("light.position", pos3);
	}
	else
	{
		this->renderer->SetVec3("light.position", this->transform_comp->GetGlobalPosition());
		position = this->transform_comp->GetGlobalPosition();
	}
	// set light parameters
	for (const auto& renderer : renderer_manager->GetAllRenderers())
	{
		renderer->SetVec3("light.position", position);
		renderer->SetVec3("light.ambient", this->ambient);
		renderer->SetVec3("light.diffuse", this->diffuse);
		renderer->SetVec3("light.specular", this->specular);
		renderer->SetFloat("light.constant", this->constant);
		renderer->SetFloat("light.linear", this->linear);
		renderer->SetFloat("light.quadratic", this->quadratic);
	}
}
