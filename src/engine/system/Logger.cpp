#include "Logger.h"
#include <iostream>

void cg::Logger::Log(const std::string& log, cg::LogLevel level)
{
	// TODO: refine the logging system, currently using std::cout
	std::cout << log << std::endl;
}

void cg::Logger::Log(const glm::vec3 vec, cg::LogLevel level /*= cg::LogLevel::DEFAULT*/)
{
	std::cout << "(" + std::to_string(vec.x) + ", " + std::to_string(vec.y) + ", " + std::to_string(vec.z) + ")" << std::endl;
}
