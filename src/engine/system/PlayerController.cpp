#include <cmath>
#include "PlayerController.h"
#include "GameInstance.h"
#include "Logger.h"

cg::PlayerController::PlayerController() :cg::ILifeCycleEvents()
{
}

std::shared_ptr<cg::Camera> cg::PlayerController::GetCameraNext()
{
	// return the next camera in the current scene by default
	auto camera = cg::GameInstance::GetInstance()->GetSceneManager()->GetCurrentScene()->GetCameraNext(this->current_camera);
	this->current_camera = camera;
	return camera;
}

std::shared_ptr<cg::Player> cg::PlayerController::AddPlayer(std::shared_ptr<cg::Player> player)
{
	this->player = player;
	return player;
}

void cg::PlayerController::Update(float delta)
{
	auto camera = cg::GameInstance::GetInstance()->GetPlayerController()->GetCurrentCamera();
	auto window_manager = cg::GameInstance::GetInstance()->GetWindowManager();
	auto window = window_manager->GetWindow();
	if (camera->GetMode() == cg::ECameraMode::FREE)
	{
		// control camera rather than player
		if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
			camera->ProcessKeyboard(EPlayerMovement::FORWARD, delta);
		if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
			camera->ProcessKeyboard(EPlayerMovement::BACKWARD, delta);
		if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
			camera->ProcessKeyboard(EPlayerMovement::LEFT, delta);
		if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
			camera->ProcessKeyboard(EPlayerMovement::RIGHT, delta);
		if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS)
			camera->ProcessKeyboard(EPlayerMovement::UP, delta);
		if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS)
			camera->ProcessKeyboard(EPlayerMovement::DOWN, delta);
	}
	else
	{
		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
			this->MoveTowards(EPlayerMovement::FORWARD, delta);
		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
			this->MoveTowards(EPlayerMovement::BACKWARD, delta);
		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
			this->MoveTowards(EPlayerMovement::LEFT, delta);
		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
			this->MoveTowards(EPlayerMovement::RIGHT, delta);
	}
}

void cg::PlayerController::MoveTowards(cg::EPlayerMovement direction, float delta_time) const
{
	// calculate player dir vectors
	if (this->current_camera->GetMode() == cg::ECameraMode::OVERHEAD)
	{
		// in overhead mode, front and right vector need to be recomputed
		this->player->SetForward(this->current_camera->GetUp());
		this->player->SetRight(glm::normalize(glm::cross(this->current_camera->GetFront(), this->current_camera->GetUp())));
	}
	auto dir = glm::vec3(1.0f);
	switch (direction)
	{
	case cg::EPlayerMovement::FORWARD:
		dir = glm::normalize(this->player->GetForward());
		break;
	case cg::EPlayerMovement::BACKWARD:
		dir = glm::normalize(-1.0f * this->player->GetForward());
		break;
	case cg::EPlayerMovement::LEFT:
		dir = glm::normalize(-1.0f * this->player->GetRight());
		break;
	case cg::EPlayerMovement::RIGHT:
		dir = glm::normalize(this->player->GetRight());
		break;
	case cg::EPlayerMovement::UP:
		dir = glm::normalize(this->player->GetUp());
		break;
	case cg::EPlayerMovement::DOWN:
		dir = glm::normalize(-1.0f * this->player->GetUp());
		break;
	default:
		break;
	}
	float velocity = this->walk_speed * delta_time;
	auto player_transform_comp = this->player->GetTransformComp();
	// only need to set local position, world position will be automatically updated
	// for a player, the local position should be equal to the world position
	player->SetGlobalPosition(player->GetGlobalPosition() + dir * velocity);
	if (this->current_camera->GetMode() == cg::ECameraMode::OVERHEAD)
	{
		auto player = this->GetPlayer();
		auto player_pos = player->GetGlobalPosition();
		this->current_camera->SetTarget(player->GetGlobalPosition());
		this->current_camera->SetPosition(glm::vec3(player_pos.x, this->current_camera->GetPosition().y, player_pos.z));
		this->light->SetGlobalPosition(player_pos.x, this->light->GetGlobalPosition().y, player_pos.z + 1.0);
	}
	if (this->current_camera->GetMode() == cg::ECameraMode::FIRST_PERSON)
	{
		auto player = this->GetPlayer();
		auto player_pos = player->GetGlobalPosition();
		this->current_camera->SetPosition(player_pos);
		this->light->SetGlobalPosition(player_pos.x, this->light->GetGlobalPosition().y, player_pos.z + 1.0);
	}
}