#include "RendererManager.h"
#include "Utility.h"

cg::RendererManager::RendererManager()
{
	this->renderers.push_back(std::make_shared<cg::Renderer>());
	this->renderers[0] = std::make_shared<cg::Renderer>(
		this->GenerateDefaultShaderCode(cg::EShaderTarget::MODEL, cg::EShaderType::VERTEX),
		this->GenerateDefaultShaderCode(cg::EShaderTarget::MODEL, cg::EShaderType::FRAGMENT)
	);
}

std::string cg::RendererManager::GenerateDefaultShaderCode(cg::EShaderTarget target, cg::EShaderType type)
{
	switch (target)
	{
	case cg::EShaderTarget::MODEL:
		switch (type)
		{
		case cg::EShaderType::VERTEX:
			return "#version 330 core\n"
				"layout(location = 0) in vec3 aPos; \n"
				"layout(location = 1) in vec3 aNormal; \n"
				"layout(location = 2) in vec2 aTexCoord; \n"
				"\n"
				"out vec2 TexCoord; \n"
				"out vec3 Normal; \n"
				"out vec3 FragPos; \n"
				"\n"
				"uniform mat4 model; \n"
				"uniform mat4 view; \n"
				"uniform mat4 projection; \n"
				"\n"
				"void main()\n"
				"{ \n"
				"gl_Position = projection * view * model * vec4(aPos, 1.0); \n"
				"TexCoord = aTexCoord; \n"
				"Normal = mat3(transpose(inverse(view * model))) * aNormal; \n"
				"FragPos = vec3(view * model * vec4(aPos, 1.0)); \n"
				"}\n";
			break;
		case cg::EShaderType::FRAGMENT:
			return "#version 330 core\n"
				"out vec4 FragColor;\n"
				"\n"
				"in vec2 TexCoord;\n"
				"in vec3 Normal;\n"
				"in vec3 FragPos;\n"
				"\n"
				"struct Material\n"
				"{\n"
				"sampler2D diffuse;\n"
				"sampler2D specular;\n"
				"float metallic;\n"
				"float subsurface;\n"
				"float roughness;\n"
				"float specularTint;\n"
				"float anisotropic;\n"
				"float sheen;\n"
				"float sheenTint;\n"
				"float clearcoat;\n"
				"float clearcoatGloss;\n"
				"};\n"
				"\n"
				"struct PointLight\n"
				"{\n"
				"vec3 position;\n"
				"vec3 direction;\n"
				"vec3 ambient;\n"
				"vec3 diffuse;\n"
				"vec3 specular;\n"
				"float intensity;\n"
				"float constant;\n"
				"float linear;\n"
				"float quadratic;\n"
				"};\n"
				"\n"
				"uniform Material material;\n"
				"uniform PointLight light;\n"
				"\n"
				"vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir)\n"
				"{\n"
				"vec3 lightDir = normalize(light.position - fragPos);\n"
				"float diff = max(dot(normal, lightDir), 0.0);\n"
				"vec3 texDiff = vec3(texture(material.diffuse, TexCoord));\n"
				"vec3 diffuse = light.diffuse * diff * texDiff;\n"
				"vec3 ambient = light.ambient * texDiff;\n"
				"vec3 reflectDir = reflect(-lightDir, normal);\n"
				"float shininess = 32.0;\n"
				"float texSpec = float(texture(material.specular, TexCoord));\n"
				"vec3 specular = light.specular * (pow(max(dot(viewDir, reflectDir), 0.0), shininess) * texSpec);"
				"float dis = length(light.position - FragPos);\n"
				"float attenuation = 1.0 / (light.constant + light.linear * dis + light.quadratic * (dis * dis));\n"
				"return attenuation * (ambient + diffuse + specular);\n"
				"}\n"
				"\n"
				"void main()\n"
				"{\n"
				"vec3 norm = normalize(Normal);\n"
				"vec3 viewDir = normalize(-FragPos);\n"
				"FragColor = vec4(CalcPointLight(light, norm, FragPos, viewDir), 1.0);\n"
				"}\n";
			break;
		default:
			return "";
			break;
		}
		break;
	case cg::EShaderTarget::LIGHT:
		switch (type)
		{
		case cg::EShaderType::VERTEX:
			return "#version 330 core\n"
				"layout (location = 0) in vec3 aPos;\n"
				"\n"
				"uniform mat4 model;\n"
				"uniform mat4 view;\n"
				"uniform mat4 projection;\n"
				"\n"
				"void main()\n"
				"{\n"
				"gl_Position = projection * view * model * vec4(aPos, 1.0);\n"
				"}\n";
			break;
		case cg::EShaderType::FRAGMENT:
			return "#version 330 core\n"
				"out vec4 FragColor;\n"
				"\n"
				"void main()\n"
				"{\n"
				"FragColor = vec4(0.0, 1.0, 0.0, 1.0);\n"
				"}\n";
			break;
		default:
			return "";
			break;
		}
		break;
	default:
		return "";
		break;
	}
}

std::shared_ptr<cg::Renderer> cg::RendererManager::GetRendererByName(const std::string& name) const
{
	auto iter = std::find_if(this->renderers.begin(), this->renderers.end(), [&name](const std::shared_ptr<cg::Renderer> renderer)
		{
			return renderer->GetName().compare(name) == 0;
		}
	);
	if (iter == this->renderers.end())
	{
		return nullptr;
	}
	else
	{
		return *iter;
	}
}

std::shared_ptr<cg::Renderer> cg::RendererManager::AddRenderer(const std::shared_ptr<cg::Renderer> renderer)
{
	// TODO: use more efficient data structures
	auto iter = std::find_if(this->renderers.begin(), this->renderers.end(), [renderer](const std::shared_ptr<cg::Renderer> r)
		{
			return renderer->GetUUID().compare(r->GetUUID()) == 0;
		}
	);
	if (iter == this->renderers.end())
	{
		this->renderers.push_back(renderer);
	}
	else
	{
		return renderer;
	}
}

std::shared_ptr<cg::Renderer> cg::RendererManager::GetDefaultRenderer() const
{
	return this->renderers[0];
}

std::shared_ptr<cg::Renderer> cg::RendererManager::GenerateRenderer(const std::string& vertex_shader_code, const std::string& fragment_shader_code)
{
	this->renderers.push_back(std::make_shared<cg::Renderer>(vertex_shader_code, fragment_shader_code));
	auto renderer = *(this->renderers.rbegin());
	return renderer;
}