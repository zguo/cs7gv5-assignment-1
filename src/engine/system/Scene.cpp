#include "Scene.h"
#include "CDynamicMesh.h"
#include "Light.h"
#include "GameInstance.h"

void cg::Scene::RenderAll(float delta_time) const
{
	// first render light
	auto instance = cg::GameInstance::GetInstance();
	auto camera = instance->GetPlayerController()->GetCurrentCamera();
	auto renderer_manager = instance->GetRendererManager();
	auto window_manager = instance->GetWindowManager();
	auto projection = glm::perspective(glm::radians(camera->GetFOV()), window_manager->GetWidth() / window_manager->GetHeight(), 0.1f, 100.0f);
	auto view = camera->GetViewMatrix();
	//auto view = glm::lookAt(glm::vec3(0.0f, 0.0f, 5.0f), glm::vec3(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	for (const auto& light : this->lights)
	{
		light->GetRenderer()->SetMat4("projection", projection);
		light->GetRenderer()->SetMat4("view", view);
		light->SetView(view);
		// render will use all shader programs from all renderers
		light->Render(delta_time);
	}
	// then draw actual models
	for (const auto& actor : this->actors)
	{	
		actor->GetRenderer()->SetMat4("projection", projection);
		actor->GetRenderer()->SetMat4("view", view);
		actor->Render(delta_time);
	}
}

std::shared_ptr<cg::Light> cg::Scene::AddLight(const std::shared_ptr<cg::Light> light)
{
	this->lights.push_back(light);
	return light;
}

std::shared_ptr<cg::Camera> cg::Scene::GetCameraNext(const std::shared_ptr<cg::Camera> current_camera)
{
	auto iter = std::find_if(this->cameras.begin(), this->cameras.end(), [current_camera](const std::shared_ptr<cg::Camera> camera)
		{
			return camera->GetUUID().compare(current_camera->GetUUID()) == 0;
		}
	);
	if (iter == this->cameras.end())
	{
		std::cout << "[WARNING] Camera: " << current_camera->GetName() << " is not in scene \"" << this->name << "\"" << std::endl;
		return current_camera;
	}
	else
	{
		iter++;
		if (iter == this->cameras.end())
			iter = this->cameras.begin();
		return *iter;
	}
}

std::shared_ptr<cg::Camera> cg::Scene::GetCameraByName(const std::string& name)
{
	auto iter = std::find_if(this->cameras.begin(), this->cameras.end(), [&name](const std::shared_ptr<cg::Camera> camera)
		{
			return camera->GetName().compare(name) == 0;
		}
	);
	if (iter == this->cameras.end())
	{
		return nullptr;
	}
	else
	{
		return *iter;
	}
}

std::shared_ptr<cg::Camera> cg::Scene::AddCamera(const std::shared_ptr<cg::Camera> camera)
{
	this->cameras.push_back(camera);
	return camera;
}