#include "Utility.h"
#include "GameInstance.h"


void cg::Utility::FramebufferSizeCallback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}

void cg::Utility::ProcessInput(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
}

void cg::Utility::ProcessInputEveryFrame(GLFWwindow* window, double delta_time)
{
	auto camera = cg::GameInstance::GetInstance()->GetPlayerController()->GetCurrentCamera();
	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		camera->ProcessKeyboard(EPlayerMovement::FORWARD, delta_time);
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		camera->ProcessKeyboard(EPlayerMovement::BACKWARD, delta_time);
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		camera->ProcessKeyboard(EPlayerMovement::LEFT, delta_time);
	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		camera->ProcessKeyboard(EPlayerMovement::RIGHT, delta_time);
	if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS)
		camera->ProcessKeyboard(EPlayerMovement::UP, delta_time);
	if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS)
		camera->ProcessKeyboard(EPlayerMovement::DOWN, delta_time);
}

GLFWwindow* cg::Utility::Init()
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "Euler Angles", NULL, NULL);
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return nullptr;
	}
	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, cg::Utility::FramebufferSizeCallback);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return nullptr;
	}

	glfwSetCursorPosCallback(window, cg::Utility::CursorPosCallbackWrapper);
	glfwSetScrollCallback(window, cg::Utility::ScrollCallbackWrapper);
	glfwSetKeyCallback(window, cg::Utility::SetKeyCallbackWrapper);

	glViewport(0, 0, SCR_WIDTH, SCR_HEIGHT);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO();
	io.Fonts->AddFontFromFileTTF("./resource/Deng.ttf", 20.0f);
	ImGui::StyleColorsDark();
	ImGui_ImplGlfw_InitForOpenGL(window, true);
	ImGui_ImplOpenGL3_Init("#version 130");

	return window;
}

std::string cg::Utility::LoadShaderCodeFromFile(const std::string file_name)
{
	std::ifstream file;
	std::string content;
	file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
	try
	{
		file.open(file_name);
		std::stringstream shader_stream;
		shader_stream << file.rdbuf();
		file.close();
		content = shader_stream.str();
		return content;
	}
	catch (std::ifstream::failure)
	{
		std::cout << "Error when trying to read shader file " << file_name << std::endl;
		return "";
	}
}

std::string cg::Utility::GenerateDefaultShaderCode(cg::EShaderTarget target, cg::EShaderType type)
{
	switch (target)
	{
	case cg::EShaderTarget::MODEL:
		switch (type)
		{
		case cg::EShaderType::VERTEX:
			return "#version 330 core\n"
				"layout(location = 0) in vec3 aPos; \n"
				"layout(location = 1) in vec3 aNormal; \n"
				"layout(location = 2) in vec2 aTexCoord; \n"
				"\n"
				"out vec2 TexCoord; \n"
				"out vec3 Normal; \n"
				"out vec3 FragPos; \n"
				"\n"
				"uniform mat4 model; \n"
				"uniform mat4 view; \n"
				"uniform mat4 projection; \n"
				"\n"
				"void main()\n"
				"{ \n"
				"gl_Position = projection * view * model * vec4(aPos, 1.0); \n"
				"TexCoord = aTexCoord; \n"
				"Normal = mat3(transpose(inverse(view * model))) * aNormal; \n"
				"FragPos = vec3(view * model * vec4(aPos, 1.0)); \n"
				"}\n";
			break;
		case cg::EShaderType::FRAGMENT:
			return "#version 330 core\n"
				"out vec4 FragColor;\n"
				"\n"
				"in vec2 TexCoord;\n"
				"in vec3 Normal;\n"
				"in vec3 FragPos;\n"
				"\n"
				"struct Material\n"
				"{\n"
				"sampler2D diffuse;\n"
				"sampler2D specular;\n"
				"float metallic;\n"
				"float subsurface;\n"
				"float roughness;\n"
				"float specularTint;\n"
				"float anisotropic;\n"
				"float sheen;\n"
				"float sheenTint;\n"
				"float clearcoat;\n"
				"float clearcoatGloss;\n"
				"};\n"
				"\n"
				"struct PointLight\n"
				"{\n"
				"vec3 position;\n"
				"vec3 direction;\n"
				"vec3 ambient;\n"
				"vec3 diffuse;\n"
				"vec3 specular;\n"
				"float intensity;\n"
				"float constant;\n"
				"float linear;\n"
				"float quadratic;\n"
				"};\n"
				"\n"
				"uniform Material material;\n"
				"uniform PointLight light;\n"
				"\n"
				"vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir)\n"
				"{\n"
				"vec3 lightDir = normalize(light.position - fragPos);\n"
				"float diff = max(dot(normal, lightDir), 0.0);\n"
				"vec3 texDiff = vec3(texture(material.diffuse, TexCoord));\n"
				"vec3 diffuse = light.diffuse * diff * texDiff;\n"
				"vec3 ambient = light.ambient * texDiff;\n"
				"vec3 reflectDir = reflect(-lightDir, normal);\n"
				"float shininess = 32.0;\n"
				"float texSpec = float(texture(material.specular, TexCoord));\n"
				"vec3 specular = light.specular * (pow(max(dot(viewDir, reflectDir), 0.0), shininess) * texSpec);"
				"float dis = length(light.position - FragPos);\n"
				"float attenuation = 1.0 / (light.constant + light.linear * dis + light.quadratic * (dis * dis));\n"
				"return attenuation * (ambient + diffuse + specular);\n"
				"}\n"
				"\n"
				"void main()\n"
				"{\n"
				"vec3 norm = normalize(Normal);\n"
				"vec3 viewDir = normalize(-FragPos);\n"
				"FragColor = vec4(CalcPointLight(light, norm, FragPos, viewDir), 1.0);\n"
				"}\n";
			break;
		default:
			return "";
			break;
		}
		break;
	case cg::EShaderTarget::LIGHT:
		switch (type)
		{
		case cg::EShaderType::VERTEX:
			return "#version 330 core\n"
				"layout (location = 0) in vec3 aPos;\n"
				"\n"
				"uniform mat4 model;\n"
				"uniform mat4 view;\n"
				"uniform mat4 projection;\n"
				"\n"
				"void main()\n"
				"{\n"
				"gl_Position = projection * view * model * vec4(aPos, 1.0);\n"
				"}\n";
			break;
		case cg::EShaderType::FRAGMENT:
			return "#version 330 core\n"
				"out vec4 FragColor;\n"
				"\n"
				"void main()\n"
				"{\n"
				"FragColor = vec4(0.0, 1.0, 0.0, 1.0);\n"
				"}\n";
			break;
		default:
			return "";
			break;
		}
		break;
	default:
		return "";
		break;
	}
}

void cg::Utility::SetInput(GLFWwindow* window)
{
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}

void cg::Utility::SetKeyCallbackWrapper(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	auto instance = cg::GameInstance::GetInstance();
	auto controller = instance->GetPlayerController();
	if (controller->SetKeyCallback != nullptr)
	{
		controller->SetKeyCallback(controller.get(), window, key, scancode, action, mods);
	}
}

void cg::Utility::CursorPosCallbackWrapper(GLFWwindow* window, double x_pos, double y_pos)
{
	auto controller = cg::GameInstance::GetInstance()->GetPlayerController();
	if (controller->CursorPosCallback != nullptr)
	{
		controller->CursorPosCallback(controller.get(), window, x_pos, y_pos);
	}
}

void cg::Utility::ScrollCallbackWrapper(GLFWwindow* window, double x_offset, double y_offset)
{
	auto controller = cg::GameInstance::GetInstance()->GetPlayerController();
	if (controller->ScrollCallback != nullptr)
	{
		controller->ScrollCallback(controller.get(), window, x_offset, y_offset);
	}
}

void cg::Utility::SetMouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
{
	auto controller = cg::GameInstance::GetInstance()->GetPlayerController();
	if (controller->SetMouseButtonCallback != nullptr)
	{
		controller->SetMouseButtonCallback(controller.get(), window, button, action, mods);
	}
}

glm::mat4 cg::Utility::CalcModelMatrix(glm::vec3 rotate, glm::vec3 translate)
{
	glm::mat4 result(1.0f);
	// rotate
	result = glm::mat4_cast(glm::normalize(glm::qua<float>(glm::radians(rotate)))) * result;
	// translate
	result = glm::translate(result, translate);
	return result;
}

glm::mat4 cg::Utility::CalcYaw(float yaw)
{
	// 绕y
	float theta = glm::radians(yaw);
	return glm::mat4(
		glm::cos(theta), 0.0f, glm::sin(theta), 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		-glm::sin(theta), 0.0f, glm::cos(theta), 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	);
}

glm::mat4 cg::Utility::CalcPitch(float pitch)
{
	// 绕z
	float theta = glm::radians(pitch);
	return glm::mat4(
		glm::cos(theta), -glm::sin(theta), 0.0f, 0.0f,
		glm::sin(theta), glm::cos(theta), 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	);
}

glm::mat4 cg::Utility::CalcRoll(float roll)
{
	// 绕x
	float theta = glm::radians(roll);
	return glm::mat4(
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, glm::cos(theta), -glm::sin(theta), 0.0f,
		0.0f, glm::sin(theta), glm::cos(theta), 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	);
}

GLuint cg::Utility::LoadTextureFromFile(const std::string& path, GLint wrap_mode, GLint mag_filter_mode, GLint min_filter_mode, cg::ETextureType type)
{
	GLuint textureID;
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_2D, textureID);

	//float borderColor[] = { 1.0f, 0.6f, 0.6f, 1.0f };
	//glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap_mode);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrap_mode);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, mag_filter_mode);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, min_filter_mode);

	int width, height, n_channels;
	int desired_channels = cg::Utility::GetChannels(type);
	unsigned char* data = stbi_load(path.c_str(), &width, &height, &n_channels, desired_channels);
	if (data)
	{
		GLint internal_format = GL_RGB8;
		GLenum format = GL_RGB;
		if (n_channels == 1)
		{
			internal_format = GL_R8;
			format = GL_RED;
		}
		else if (n_channels == 3)
		{
			internal_format = GL_RGB8;
			format = GL_RGB;
		}
		else if (n_channels == 4)
		{
			internal_format = GL_RGBA8;
			format = GL_RGBA;
		}
		glTexImage2D(GL_TEXTURE_2D, 0, internal_format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		std::cout << "Failed to load texture" << std::endl;
	}
	stbi_image_free(data);
	glBindTexture(GL_TEXTURE_2D, 0);
	return textureID;
}

GLuint cg::Utility::LoadTextureEmbedded(const aiTexture* texture, GLint wrap_mode, GLint mag_filter_mode, GLint min_filter_mode, cg::ETextureType type)
{
	if (!texture)
	{
		return 0;
	}
	GLuint tex_id = 0;
	glGenTextures(1, &tex_id);
	glBindTexture(GL_TEXTURE_2D, tex_id);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap_mode);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrap_mode);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, mag_filter_mode);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, min_filter_mode);

	int width, height, n_channels;
	int desired_channels = cg::Utility::GetChannels(type);
	unsigned char* data = nullptr;
	if (texture->mHeight == 0)
	{
		// texture is compressed, see more detail at https://assimp-docs.readthedocs.io/en/latest/usage/use_the_lib.html#textures
		data = stbi_load_from_memory(reinterpret_cast<unsigned char*>(texture->pcData), texture->mWidth, &width, &height, &n_channels, desired_channels);
	}
	else
	{
		data = stbi_load_from_memory(reinterpret_cast<unsigned char*>(texture->pcData), texture->mWidth * texture->mHeight, &width, &height, &n_channels, desired_channels);
	}
	if (data)
	{
		GLint internal_format = GL_RGB8;
		GLenum format = GL_RGB;
		if (n_channels == 1)
		{
			internal_format = GL_R8;
			format = GL_RED;
		}
		else if (n_channels == 3)
		{
			internal_format = GL_RGB8;
			format = GL_RGB;
		}
		else if (n_channels == 4)
		{
			internal_format = GL_RGBA8;
			format = GL_RGBA;
		}
		glTexImage2D(GL_TEXTURE_2D, 0, internal_format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		std::cout << "Failed to load texture" << std::endl;
	}
	stbi_image_free(data);
	glBindTexture(GL_TEXTURE_2D, 0);
	return tex_id;
}

cg::Texture cg::Utility::LoadDefaultTexture()
{
	std::string path = ROOT_DIR + "resource/default/diffuse.png";
	GLuint id = cg::Utility::LoadTextureFromFile(path, GL_REPEAT, GL_LINEAR, GL_LINEAR, cg::ETextureType::DIFFUSE_MAP);
	return cg::Texture(path, id, ETextureType::DIFFUSE_MAP);
}

int cg::Utility::GetChannels(cg::ETextureType type)
{
	if (type == cg::ETextureType::DIFFUSE_MAP)
		return 0;
	else if (type == cg::ETextureType::SPECULAR_MAP)
		return 0; // TODO: 暂且读取所有channel
}
