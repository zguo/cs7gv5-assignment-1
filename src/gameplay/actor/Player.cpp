#include "Player.h"
#include "Logger.h"
#include "GameInstance.h"

cg::Player::Player(const std::string& model_file /*= ""*/) : cg::DynamicModel()
{
	this->AddMeshComponent(model_file);
}

void cg::Player::Update(float delta)
{
	// update position of FP camera
	auto instance = cg::GameInstance::GetInstance();
	auto controller = instance->GetPlayerController();
	auto camera = controller->GetCurrentCamera();
	auto scene = instance->GetSceneManager()->GetCurrentScene();
	if (camera->GetMode() == cg::ECameraMode::FIRST_PERSON)
	{
		camera->SetPosition(this->GetGlobalPosition());
	}
}
