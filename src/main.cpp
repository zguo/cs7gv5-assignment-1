#pragma comment(lib, "bcrypt.lib")

#include "common.h"

#include "assimp/Importer.hpp"
#include "assimp/scene.h"
#include "assimp/postprocess.h"
#include "stb/stb_image.h"
#include "irrKlang/irrKlang.h"

#include <iostream>

#include "Renderer.h"
#include "ACamera.h"
#include "CCamera.h"
#include "Utility.h"
#include "SVertex.h"
#include "SMaterial.h"
#include "STexture.h"
#include "AStaticModel.h"
#include "Light.h"
#include "Scene.h"
#include "GameInstance.h"
#include "PlayerController.h"
#include "Logger.h"
#include "SkyBox.h"


const std::string ROOT_DIR = R"(./)";

int main()
{
	/****************************************/ // [begin] init
	// create game instance
	auto instance = cg::GameInstance::GetInstance();
	auto scene = instance->GetSceneManager()->GetCurrentScene();
	// add sky box
	auto skybox = std::make_shared<cg::SkyBox>("resource/skybox/", "shader/skybox.vert", "shader/skybox.frag");
	scene->AddActor<cg::SkyBox>(skybox);
	// add player and player controller
	auto controller = instance->GetPlayerController();
	auto player = controller->AddPlayer(std::make_shared<cg::Player>(ROOT_DIR + "resource/player/player.fbx"));
	player->SetGlobalPosition(0.0f, 0.0f, 5.0f);
	player->SetLocalScale(glm::vec3(.005f, .005f, .005f));
	scene->AddActor<cg::Player>(player);
	controller->CursorPosCallback = [](cg::PlayerController* self, GLFWwindow* window, double x_pos, double y_pos)
	{
		auto camera = self->GetCurrentCamera();
		if (camera->CursorPosCallback != nullptr)
		{
			camera->CursorPosCallback(self, window, x_pos, y_pos);
		}
	};
	controller->ScrollCallback = [](cg::PlayerController* self, GLFWwindow* window, double x_offset, double y_offset)
	{
		auto camera = self->GetCurrentCamera();
		if (camera->ScrollCallback != nullptr)
		{
			camera->ScrollCallback(self, window, x_offset, y_offset);
		}
	};
	controller->SetKeyCallback = [](cg::PlayerController* self, GLFWwindow* window, int key, int scancode, int action, int mods)
	{
		if (key == GLFW_KEY_TAB && action == GLFW_PRESS)
		{
			auto current_camera = self->GetCurrentCamera();
			current_camera->SetFirstMouse(true);
			self->GetCameraNext();
		}
		if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
			glfwSetWindowShouldClose(window, true);
	};
	/****************************************/ // [end] init

	/****************************************/ // [begin] model and lights
	// add models
	auto model = scene->AddActor<cg::DynamicModel>(std::make_shared<cg::DynamicModel>());
	auto plane = model->AddMeshComponent(std::make_shared<cg::DynamicMeshComponent>(ROOT_DIR + "resource/plane/plane.fbx"));
	//auto propeller = model->AddMeshComponent(std::make_shared<cg::DynamicMeshComponent>(ROOT_DIR + "resource/plane/propeller.fbx"));
	//propeller->SetLocalPosition(-4.0f, 0.0f, 0.0f);
	//propeller->SetAnimation(std::make_shared<cg::Animation>(propeller, glm::vec3(1.0f, 0.0f, 0.0f), 0.5f, true, true));
	//propeller->EnableAnimation();
	// add lights
	auto light = scene->AddLight(std::make_shared<cg::Light>(cg::ELightType::POINT));
	light->SetConstant(5.0f);
	light->SetLinear(0.09f);
	light->SetQuadratic(0.032f);
	light->SetAmbient(glm::vec3(3.0f));
	light->SetDiffuse(glm::vec3(1.0f));
	light->SetSpecular(glm::vec3(1.0f));
	light->SetGlobalPosition(glm::vec3(0.0f, 1.0f, 5.0f));
	controller->AddLight(light);
	/****************************************/ // [end] models and lights

	/****************************************/ // [begin] cameras
	// add cameras
	// first-person
	const auto camera_fp = std::make_shared<cg::Camera>(
		player->GetGlobalPosition(), // position
		cg::ECameraMode::FIRST_PERSON);
	camera_fp->CursorPosCallback = [](cg::PlayerController* self, GLFWwindow* window, double x_pos, double y_pos)
	{
		const auto camera = self->GetCurrentCamera();
		if (camera->GetFirstMouse())
		{
			camera->SetLastX(x_pos);
			camera->SetLastY(y_pos);
			camera->SetFirstMouse(false);
		}
		float x_offset = x_pos - camera->GetLastX();
		float y_offset = camera->GetLastY() - y_pos;
		camera->SetLastX(x_pos);
		camera->SetLastY(y_pos);
		float sensitivity = cg::SENSITIVITY;
		x_offset *= cg::SENSITIVITY;
		y_offset *= cg::SENSITIVITY;
		camera->SetYaw(camera->GetYaw() + x_offset);
		camera->SetPitch(camera->GetPitch() > 89.0f ? 89.0f : (camera->GetPitch() < -89.0f ? -89.0f : camera->GetPitch() + y_offset));
		glm::vec3 front(1.0f);
		front.x = std::cos(glm::radians(camera->GetPitch())) * std::cos(glm::radians(camera->GetYaw()));
		front.y = std::sin(glm::radians(camera->GetPitch()));
		front.z = std::cos(glm::radians(camera->GetPitch())) * std::sin(glm::radians(camera->GetYaw()));
		camera->SetFront(glm::normalize(front));
		camera->SetRight(glm::normalize(glm::cross(camera->GetFront(), glm::vec3(0.0f, 1.0f, 0.0f))));
		camera->SetUp(glm::normalize(glm::cross(camera->GetRight(), camera->GetFront())));
		auto prev_dir = self->GetPlayer()->GetForward();
		prev_dir.y = 0;
		auto new_dir = camera->GetFront();
		new_dir.y = 0;
		self->GetPlayer()->SetForward(glm::normalize(new_dir));
		self->GetPlayer()->SetRight(glm::normalize(glm::cross(self->GetPlayer()->GetForward(), self->GetPlayer()->GetUp())));
	};
	camera_fp->ScrollCallback = [](cg::PlayerController* self, GLFWwindow* window, double x_offset, double y_offset) {};
	// third-person
	auto camera_tp = std::make_shared<cg::Camera>(
		glm::vec3(0.0f, 0.0f, 0.0f),
		cg::ECameraMode::THIRD_PERSON);
	camera_tp->SetDistance(5.0f);
	camera_tp->SetTarget(player->GetTransformComp()->GetGlobalPosition());
	// callback of tp camera
	camera_tp->CursorPosCallback = [](cg::PlayerController* self, GLFWwindow* window, double x_pos, double y_pos)
	{
		auto camera = self->GetCurrentCamera();
		if (camera->GetFirstMouse())
		{
			camera->SetLastX(x_pos);
			camera->SetLastY(y_pos);
			camera->SetFirstMouse(false);
		}
		float x_offset = x_pos - camera->GetLastX();
		float y_offset = camera->GetLastY() - y_pos;
		camera->SetLastX(x_pos);
		camera->SetLastY(y_pos);
		x_offset *= cg::SENSITIVITY;
		y_offset *= cg::SENSITIVITY;
		// set position of camera
		camera->SetYaw(camera->GetYaw() + x_offset);
		camera->SetPitch(camera->GetPitch() > 89.0f ? 89.0f : (camera->GetPitch() < -89.0f ? -89.0f : camera->GetPitch() + y_offset));
		glm::vec3 pos(1.0f);
		float d = camera->GetDistance();
		float sin_theta = std::sin(glm::radians(camera->GetPitch()));
		float cos_theta = std::cos(glm::radians(camera->GetPitch()));
		float sin_phi = std::sin(glm::radians(camera->GetYaw()));
		float cos_phi = std::cos(glm::radians(camera->GetYaw()));
		pos.y = sin_theta * d * -1;
		pos.x = cos_theta * sin_phi * d;
		pos.z = cos_theta * cos_phi * d * -1;
		camera->SetPosition(pos + camera->GetTarget());
		camera->SetFront(glm::normalize(camera->GetTarget() - camera->GetPosition()));
		camera->SetRight(glm::normalize(glm::cross(camera->GetFront(), glm::vec3(0.0f, 1.0f, 0.0f))));
		camera->SetUp(glm::normalize(glm::cross(camera->GetRight(), camera->GetFront())));
	};
	camera_tp->ScrollCallback = [](cg::PlayerController* self, GLFWwindow* window, double x_offset, double y_offset)
	{
		auto camera = self->GetCurrentCamera();
		float fov = camera->GetFOV();
		if (fov >= 1.0f && fov <= 45.0f)
			fov -= y_offset;
		if (fov <= 1.0f)
			fov = 1.0f;
		if (fov >= 45.0f)
			fov = 45.0f;
		camera->SetFOV(fov);
	};
	scene->AddCamera(camera_fp);
	scene->AddCamera(camera_tp);
	controller->SetCurrentCamera(camera_fp);
	/****************************************/ // [end] cameras

	instance->Run();

	return 0;
}